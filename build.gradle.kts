plugins {
    id("org.jetbrains.kotlin.platform.common") version "1.2.51" apply false
    id("org.jetbrains.kotlin.platform.jvm") version "1.2.51" apply false
    id("org.jetbrains.kotlin.platform.native") version "0.9-dev-2873" apply false
    id("com.github.johnrengelman.shadow") version "2.0.4" apply false
}


subprojects {
    group = "me.r4zzz4k.girkt"
    version = "1.0-SNAPSHOT"

    repositories {
        jcenter()
        maven(url = "https://dl.bintray.com/jetbrains/kotlin-native-dependencies")
        maven(url = "https://dl.bintray.com/kotlin/kotlin-dev")
    }
}
