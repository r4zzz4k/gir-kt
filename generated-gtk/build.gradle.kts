import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "me.r4zzz4k.girkt"
version = "1.0-SNAPSHOT"

//apply(plugin('org.jetbrains.kotlin.platform.common'))
plugins {
    id("org.jetbrains.kotlin.platform.jvm")
    id("java-library")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(project(":gir-common"))
    implementation(project(":gir-jvm"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
