rootProject.name = "gir-kt"

enableFeaturePreview("GRADLE_METADATA")

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
        maven(url = "https://dl.bintray.com/jetbrains/kotlin-native-dependencies")
        maven(url = "https://dl.bintray.com/kotlin/kotlin-dev")
    }

    resolutionStrategy {
        eachPlugin {
            val id = requested.id.id
            when {
                id == "org.jetbrains.kotlin.platform.native" ->
                    "org.jetbrains.kotlin:kotlin-native-gradle-plugin"
                id.startsWith("org.jetbrains.kotlin.platform.") ->
                    "org.jetbrains.kotlin:kotlin-gradle-plugin"
                else -> null
            }?.let { useModule("$it:${requested.version}") }
        }
    }
}

include(":gir-common")
include(":gir-jvm")
include(":gir-jvm-cpp")
include(":introspect-jvm")
include(":generated-gtk")
include(":sample-jvm")
