#include "gi.h"
#include <cstring>
#include <cstdio>

// https://github.com/creationix/node-gir/blob/da0b211c1d0b2243edb9cb86b293706e0b94ee14/src/arguments.cc

static void printTypeError(JNIEnv* env, char const* argName, GITypeTag expected, jobject actual) {
    fprintf(stderr, "Invalid type passed for %s: expected %s, got ", argName, g_type_tag_to_string(expected));
    printClassName(stderr, env, actual);
    fputs("\n", stderr);
}
static void printUnimplementedError(JNIEnv* env, char const* argName, GITypeTag expected, jobject actual) {
    fprintf(stderr, "Unimplemented type for %s: %s, got ", argName, g_type_tag_to_string(expected));
    printClassName(stderr, env, actual);
    fputs("\n", stderr);
}

class GirProvider {
public:
    virtual ~GirProvider() = default;
    virtual jobject provideJObject() = 0;
};

class PlainProvider: public GirProvider {
    jobject output;
public:
    explicit PlainProvider(jobject output): output{output} {}
    virtual ~PlainProvider() = default;
    jobject provideJObject() override { return output; }
};

class ArrayProvider: public GirProvider {
    JNIEnv* env;
    jobject output;
    int iter;
public:
    explicit ArrayProvider(JNIEnv* env, jobject output): env{env}, output{output}, iter{0} {}
    virtual ~ArrayProvider() = default;
    jobject provideJObject() override { return env->CallStaticObjectMethod(Array, Array_get, output, iter++); }
};

class GirCollector {
public:
    virtual ~GirCollector() = default;
    virtual void collectNull() = 0;
    virtual void collectBoolean(bool input) = 0;
    virtual void collectInt8(int8_t input) = 0;
    virtual void collectUint8(uint8_t input) = 0;
    virtual void collectInt16(int16_t input) = 0;
    virtual void collectUint16(uint16_t input) = 0;
    virtual void collectInt32(int32_t input) = 0;
    virtual void collectUint32(uint32_t input) = 0;
    virtual void collectInt64(int64_t input) = 0;
    virtual void collectUint64(uint64_t input) = 0;
    virtual void collectGType(GType input) = 0;
    virtual void collectFloat(float input) = 0;
    virtual void collectDouble(double input) = 0;
    virtual void collectString(char const* input) = 0;
    virtual void collectPointer(void* input) = 0;
};

class GiArgumentCollector: public GirCollector {
    GIArgument* output;
public:
    explicit GiArgumentCollector(GIArgument* output): output{output} {
        output->v_string = nullptr;
    }
    virtual ~GiArgumentCollector() = default;

    void collectNull() override { output->v_pointer = nullptr; }
    void collectBoolean(bool input) override { output->v_boolean = input; }
    void collectInt8(int8_t input) override { output->v_int8 = input; }
    void collectUint8(uint8_t input) override { output->v_uint8 = input; }
    void collectInt16(int16_t input) override { output->v_int16 = input; }
    void collectUint16(uint16_t input) override { output->v_uint16 = input; }
    void collectInt32(int32_t input) override { output->v_int32 = input; }
    void collectUint32(uint32_t input) override { output->v_uint32 = input; }
    void collectInt64(int64_t input) override { output->v_int64 = input; }
    void collectUint64(uint64_t input) override { output->v_uint64 = input; }
    void collectGType(GType input) override { output->v_long = input; }
    void collectFloat(float input) override { output->v_float = input; }
    void collectDouble(double input) override { output->v_double = input; }
    void collectString(char const* input) override { output->v_string = g_strdup(input); }
    void collectPointer(void* input) override { output->v_pointer = input; }
};

class CArrayCollector: public GirCollector {
    void** output;
    size_t size;

    template<typename T> void append(T value) {
        if(!*output) {
            *output = new T[size];
        }
        *reinterpret_cast<T*>(*output) = value;
        *output = reinterpret_cast<T*>(*output) + 1;
    }
public:
    CArrayCollector(void** output, size_t size): output{output}, size{size} {}

    void collectNull() override { append<void*>(nullptr); }
    void collectBoolean(bool input) override { append<bool>(input); }
    void collectInt8(int8_t input) override { append<int8_t>(input); }
    void collectUint8(uint8_t input) override { append<uint8_t>(input); }
    void collectInt16(int16_t input) override { append<int16_t>(input); }
    void collectUint16(uint16_t input) override { append<uint16_t>(input); }
    void collectInt32(int32_t input) override { append<int32_t>(input); }
    void collectUint32(uint32_t input) override { append<uint32_t>(input); }
    void collectInt64(int64_t input) override { append<int64_t>(input); }
    void collectUint64(uint64_t input) override { append<uint64_t>(input); }
    void collectGType(GType input) override { append<long>(input); }
    void collectFloat(float input) override { append<float>(input); }
    void collectDouble(double input) override { append<double>(input); }
    void collectString(char const* input) override { append<char const*>(g_strdup(input)); }
    void collectPointer(void* input) override { append<void*>(input); }
};

bool convertToCollector(
        JNIEnv* env,
        GirProvider& provider,
        GirCollector& collector,
        GITypeInfo* typeInfo,
        bool mayBeNull,
        char const* argName) {
    auto typeTag = g_type_info_get_tag(typeInfo);

    jobject input = provider.provideJObject();

    if(input == nullptr) {
        if(mayBeNull) {
            collector.collectNull();
            return true;
        } else {
            fputs("Got null for non-null argument\n", stderr);
            return false;
        }
    }

    switch(typeTag) {
        case GI_TYPE_TAG_BOOLEAN: {
            if(env->IsInstanceOf(input, Boolean)) {
                collector.collectBoolean(env->CallBooleanMethod(input, Boolean_booleanValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_INT8: {
            if(env->IsInstanceOf(input, Integer)) {
                collector.collectInt8(env->CallIntMethod(input, Integer_intValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_UINT8: {
            if(env->IsInstanceOf(input, Integer)) {
                collector.collectUint8(env->CallIntMethod(input, Integer_intValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_INT16: {
            if(env->IsInstanceOf(input, Integer)) {
                collector.collectInt16(env->CallIntMethod(input, Integer_intValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_UINT16: {
            if(env->IsInstanceOf(input, Integer)) {
                collector.collectUint16(env->CallIntMethod(input, Integer_intValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_INT32: {
            if(env->IsInstanceOf(input, Integer)) {
                collector.collectInt32(env->CallIntMethod(input, Integer_intValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_UINT32: {
            if(env->IsInstanceOf(input, Integer)) {
                collector.collectUint32(env->CallIntMethod(input, Integer_intValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_INT64: {
            if(env->IsInstanceOf(input, Long)) {
                collector.collectInt64(env->CallLongMethod(input, Long_longValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_UINT64: {
            if(env->IsInstanceOf(input, Long)) {
                collector.collectUint64(env->CallLongMethod(input, Long_longValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_GTYPE: {
            if(env->IsInstanceOf(input, GTypeRef)) {
                // TODO check if GType has other size on other platforms
                collector.collectGType(env->GetLongField(input, GTypeRef_ptr));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_FLOAT: {
            if(env->IsInstanceOf(input, Float)) {
                collector.collectFloat(env->CallFloatMethod(input, Float_floatValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_DOUBLE: {
            if(env->IsInstanceOf(input, Double)) {
                collector.collectDouble(env->CallDoubleMethod(input, Double_doubleValue));
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_UTF8:
        case GI_TYPE_TAG_FILENAME: {
            if(env->IsInstanceOf(input, String)) {
                auto str = env->GetStringUTFChars((jstring) input, nullptr);
                // TODO toRelease.emplace_back(std::make_pair((jstring) valueRef, str));
                collector.collectString(str);
                return true;
            }
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_GLIST:
        case GI_TYPE_TAG_GSLIST: {
            GITypeInfo* paramTypeInfo = g_type_info_get_param_type(typeInfo, 0);
            auto paramTypeTag = g_type_info_get_tag(paramTypeInfo);
            // TODO
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_ARRAY: {
            GITypeInfo* paramTypeInfo = g_type_info_get_param_type(typeInfo, 0);
            auto paramTypeTag = g_type_info_get_tag(paramTypeInfo);

            GIArrayType arrType = g_type_info_get_array_type(typeInfo);

            if(env->IsInstanceOf(input, String) && arrType == GI_ARRAY_TYPE_C) {
                auto str = env->GetStringUTFChars((jstring) input, nullptr);
                // TODO toRelease.emplace_back(std::make_pair((jstring) valueRef, str));
                collector.collectString(str);

                g_base_info_unref((GIBaseInfo*) paramTypeInfo);
                return true;
            }

            jobject inputClass = env->CallObjectMethod(input, Object_getClass);

            if(env->CallBooleanMethod(inputClass, Class_isArray)) {
                fprintf(stderr, "##### isArray: true; input: %snull, type: %s\n", input ? "not " : "", g_type_tag_to_string(g_type_info_get_tag(paramTypeInfo)));
                auto size = env->CallStaticIntMethod(Array, Array_getLength, input);
                fprintf(stderr, "##### input size: %d\n", size);

                if(size == 0) {
                    collector.collectNull();
                    return true;
                }

                //auto element = env->CallObjectMethod(input, Array_get, 0);

                switch(arrType) {
                    case GI_ARRAY_TYPE_C: {
                        ArrayProvider arrayProvider(env, input);

                        void* ptr = nullptr;
                        CArrayCollector arrayCollector(&ptr, size);
                        if(convertToCollector(env, arrayProvider, arrayCollector, paramTypeInfo, true, argName)) {
                            collector.collectPointer(ptr);
                            return true;
                        }

                        //convertGiCollection(env, )
                        break;
                    }
                }
            }




            // TODO
            //printUnimplementedError(env, argName, typeTag, input);
            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_GHASH: {
            if(env->IsInstanceOf(input, Map)) {
                GITypeInfo* keyInfo = g_type_info_get_param_type(typeInfo, 0);
                GITypeInfo* valueInfo = g_type_info_get_param_type(typeInfo, 1);

                // TODO

                g_base_info_unref((GIBaseInfo*) keyInfo);
                g_base_info_unref((GIBaseInfo*) valueInfo);
            }

            printTypeError(env, argName, typeTag, input);
            break;
        }
        case GI_TYPE_TAG_INTERFACE: {
            GIBaseInfo* interfaceInfo = g_type_info_get_interface(typeInfo);
            GIInfoType interfaceType = g_base_info_get_type(interfaceInfo);
            fprintf(stderr, "####### Converting GI_TYPE_TAG_INTERFACE %s\n", g_info_type_to_string(interfaceType));

            //GType gType;
            switch(interfaceType) {
                case GI_INFO_TYPE_STRUCT:
                case GI_INFO_TYPE_ENUM:
                case GI_INFO_TYPE_OBJECT:
                case GI_INFO_TYPE_INTERFACE:
                case GI_INFO_TYPE_UNION:
                case GI_INFO_TYPE_BOXED: {
                    if(g_registered_type_info_get_g_type(interfaceInfo) == G_TYPE_VALUE) {
                        fprintf(stderr, "####### Converting GValue\n");
                        GValue value = G_VALUE_INIT;
                        if (!convertGiValue(env, input, &value)) {
                            fprintf(stderr, "####### Converting GValue FAILED\n");
                            printTypeError(env, argName, typeTag, input);
                            break;
                        }
                        fprintf(stderr, "####### Converting GValue SUCCESSFUL\n");
                        collector.collectPointer(g_boxed_copy(G_TYPE_VALUE, &value));
                    }
                    /*fprintf(stderr, "####### Converting struct, enum, etc %s %s\n", g_base_info_get_name(interfaceInfo), t ==  ? "same" : "not same");
                    collector.collectPointer(getNativePtr<void>(env, input, GObjectRef_ptr));*/
                    //gType = g_registered_type_info_get_g_type((GIRegisteredTypeInfo*) interfaceInfo);
                    break;
                }
                case GI_INFO_TYPE_VALUE: {
                    /*fprintf(stderr, "####### Converting GValue\n");
                    //gType = G_TYPE_VALUE;
                    GValue value = G_VALUE_INIT;
                    if (!convertGiValue(env, input, &value)) {
                        printTypeError(env, argName, typeTag, input);
                        break;
                    }
                    collector.collectPointer(g_boxed_copy(G_TYPE_VALUE, &value));
                    g_value_unset(&value);*/
                    break;
                }

                default:
                    //gType = G_TYPE_NONE;
                    break;
            }

            // TODO
            /*if(g_type_is_a(gtype, G_TYPE_OBJECT)) {
                if(!v->IsObject()) { return false; }
                GIRObject *gir_object = Nan::ObjectWrap::Unwrap<GIRObject>(v->ToObject());
                arg->v_pointer = gir_object->obj;
                return true;
            }
            if(g_type_is_a(gtype, G_TYPE_VALUE)) {
                GValue gvalue = {0, {{0}}};
                if(!GIRValue::ToGValue(v, G_TYPE_INVALID, &gvalue)) {
                    return false;
                }
                //FIXME I've to free this somewhere
                arg->v_pointer = g_boxed_copy(G_TYPE_VALUE, &gvalue);
                g_value_unset(&gvalue);
                return true;
            }*/

            printTypeError(env, argName, typeTag, input);
            break;
        }
        default: {
            printUnimplementedError(env, argName, typeTag, input);
        }
    }
    return false;
}
bool convertGiArgument(JNIEnv* env, jobject input, GIArgument* output, GITypeInfo* typeInfo, bool mayBeNull, char const* argName) {
    PlainProvider provider { input };
    GiArgumentCollector collector { output };
    return convertToCollector(env, provider, collector, typeInfo, mayBeNull, argName);
}

jobject convertGiArgument(JNIEnv* env, GIArgument* input, GITypeInfo* typeInfo, char const* argName) {
    auto typeTag = g_type_info_get_tag(typeInfo);
    switch(typeTag) {
        case GI_TYPE_TAG_VOID:
            return nullptr;
        case GI_TYPE_TAG_BOOLEAN:
            return env->CallObjectMethod(Boolean, Boolean_valueOf, input->v_boolean);
        case GI_TYPE_TAG_INT8:
            return env->CallObjectMethod(Integer, Integer_valueOf, input->v_int8);
        case GI_TYPE_TAG_UINT8:
            return env->CallObjectMethod(Integer, Integer_valueOf, input->v_uint8);
        case GI_TYPE_TAG_INT16:
            return env->CallObjectMethod(Integer, Integer_valueOf, input->v_int16);
        case GI_TYPE_TAG_UINT16:
            return env->CallObjectMethod(Integer, Integer_valueOf, input->v_uint16);
        case GI_TYPE_TAG_INT32:
            return env->CallObjectMethod(Integer, Integer_valueOf, input->v_int32);
        case GI_TYPE_TAG_UINT32:
            return env->CallObjectMethod(Integer, Integer_valueOf, input->v_uint32);
        case GI_TYPE_TAG_INT64:
            return env->CallObjectMethod(Long, Long_valueOf, input->v_int64);
        case GI_TYPE_TAG_UINT64:
            return env->CallObjectMethod(Long, Long_valueOf, input->v_uint64);
        case GI_TYPE_TAG_FLOAT:
            return env->CallObjectMethod(Float, Float_valueOf, input->v_float);
        case GI_TYPE_TAG_DOUBLE:
            return env->CallObjectMethod(Double, Double_valueOf, input->v_double);
        case GI_TYPE_TAG_GTYPE:
            return constructObjectWithNativeLong(env, GTypeRef, GTypeRef_ctor, input->v_long);
        case GI_TYPE_TAG_UTF8:
        case GI_TYPE_TAG_FILENAME:
            return env->NewStringUTF((char const*) input->v_pointer);
        case GI_TYPE_TAG_INTERFACE: {
            GIBaseInfo* interfaceInfo = g_type_info_get_interface(typeInfo);
            GIInfoType interfaceType = g_base_info_get_type(interfaceInfo);

            //GType gType;
            switch (interfaceType) {
                case GI_INFO_TYPE_STRUCT:
                case GI_INFO_TYPE_ENUM:
                case GI_INFO_TYPE_OBJECT:
                case GI_INFO_TYPE_INTERFACE:
                case GI_INFO_TYPE_UNION:
                case GI_INFO_TYPE_BOXED:
                    return constructObjectWithNativePtr(env, GObjectRef, GObjectRef_ctor, input->v_pointer);
                    //gType = g_registered_type_info_get_g_type((GIRegisteredTypeInfo*) interfaceInfo);
                    break;
                case GI_INFO_TYPE_VALUE: {
                    //gType = G_TYPE_VALUE;
                    /*GValue value = G_VALUE_INIT;
                    if (!convertGiValue(env, input, &value)) {
                        printTypeError(env, argName, typeTag, input);
                        break;
                    }
                    collector->v_pointer = g_boxed_copy(G_TYPE_VALUE, &value);
                    g_value_unset(&value);*/
                    break;
                }

                default:
                    //gType = G_TYPE_NONE;
                    break;
            }
            //return constructObjectWithNativeLong(env, GTypeRef, GTypeRef_ctor, input->v_long);
        }
    }
    printUnimplementedError(env, argName, typeTag, nullptr);

    return nullptr;
}
