#include "gi.h"

JNIEXPORT jobject JNICALL GiRepositoryKt_giRepository(JNIEnv* env, jclass clazz) {
    auto repo = g_irepository_get_default();
    return constructObjectWithNativePtr(env, GiRepository, GiRepository_ctor, repo);
}

JNIEXPORT jstring JNICALL EnumsKt_giInfoTypeToString(JNIEnv* env, jclass clazz, jobject thiz) {
    auto type = convertGiInfoType(env, thiz);
    auto resultPtr = g_info_type_to_string(type);
    return env->NewStringUTF(resultPtr);
}

JNIEXPORT jstring JNICALL EnumsKt_giTypeTagToString(JNIEnv* env, jclass clazz, jobject thiz) {
    auto type = convertGiTypeTag(env, thiz);
    auto resultPtr = g_type_tag_to_string(type);
    return env->NewStringUTF(resultPtr);
}
