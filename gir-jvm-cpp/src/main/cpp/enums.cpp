#include "gi.h"
#include <cstring>

jobject convertGSignalFlags(JNIEnv* env, GSignalFlags input) {
    return populateSet(env, [&](auto add) {
        if(input & G_SIGNAL_RUN_FIRST) add(GlibGSignalFlags_RUN_FIRST);
        if(input & G_SIGNAL_RUN_LAST) add(GlibGSignalFlags_RUN_LAST);
        if(input & G_SIGNAL_RUN_CLEANUP) add(GlibGSignalFlags_RUN_CLEANUP);
        if(input & G_SIGNAL_NO_RECURSE) add(GlibGSignalFlags_NO_RECURSE);
        if(input & G_SIGNAL_DETAILED) add(GlibGSignalFlags_DETAILED);
        if(input & G_SIGNAL_ACTION) add(GlibGSignalFlags_ACTION);
        if(input & G_SIGNAL_NO_HOOKS) add(GlibGSignalFlags_NO_HOOKS);
        if(input & G_SIGNAL_MUST_COLLECT) add(GlibGSignalFlags_MUST_COLLECT);
        if(input & G_SIGNAL_DEPRECATED) add(GlibGSignalFlags_DEPRECATED);
    });
}

jobject convertGiArrayType(JNIEnv* env, GIArrayType input) {
    switch(input) {
        case GI_ARRAY_TYPE_C: return GiArrayType_C;
        case GI_ARRAY_TYPE_ARRAY: return GiArrayType_ARRAY;
        case GI_ARRAY_TYPE_PTR_ARRAY: return GiArrayType_PTR_ARRAY;
        case GI_ARRAY_TYPE_BYTE_ARRAY: return GiArrayType_BYTE_ARRAY;
        default: return nullptr;
    }
}

jobject convertGiDirection(JNIEnv* env, GIDirection input) {
    switch(input) {
        case GI_DIRECTION_IN: return GiDirection_IN;
        case GI_DIRECTION_OUT: return GiDirection_OUT;
        case GI_DIRECTION_INOUT: return GiDirection_INOUT;
        default: return nullptr;
    }
}

jobject convertGiFieldInfoFlags(JNIEnv* env, GIFieldInfoFlags input) {
    return populateSet(env, [&](auto add) {
        if(input & GI_FIELD_IS_READABLE) add(GiFieldInfoFlags_IS_READABLE);
        if(input & GI_FIELD_IS_WRITABLE) add(GiFieldInfoFlags_IS_WRITABLE);
    });
}

jobject convertGiVFuncInfoFlags(JNIEnv* env, GIVFuncInfoFlags input) {
    return populateSet(env, [&](auto add) {
        if(input & GI_VFUNC_MUST_CHAIN_UP) add(GiVFuncInfoFlags_MUST_CHAIN_UP);
        if(input & GI_VFUNC_MUST_OVERRIDE) add(GiVFuncInfoFlags_MUST_OVERRIDE);
        if(input & GI_VFUNC_MUST_NOT_OVERRIDE) add(GiVFuncInfoFlags_MUST_NOT_OVERRIDE);
        if(input & GI_VFUNC_THROWS) add(GiVFuncInfoFlags_THROWS);
    });
}

jobject convertGiScopeType(JNIEnv* env, GIScopeType input) {
    switch(input) {
        case GI_SCOPE_TYPE_INVALID: return GiScopeType_INVALID;
        case GI_SCOPE_TYPE_CALL: return GiScopeType_CALL;
        case GI_SCOPE_TYPE_ASYNC: return GiScopeType_ASYNC;
        case GI_SCOPE_TYPE_NOTIFIED: return GiScopeType_NOTIFIED;
        default: return nullptr;
    }
}
jobject convertGiTransfer(JNIEnv* env, GITransfer input) {
    switch(input) {
        case GI_TRANSFER_NOTHING: return GiTransfer_NOTHING;
        case GI_TRANSFER_CONTAINER: return GiTransfer_CONTAINER;
        case GI_TRANSFER_EVERYTHING: return GiTransfer_EVERYTHING;
        default: return nullptr;
    }
}

jobject convertGiInfoType(JNIEnv* env, GIInfoType input) {
    switch(input) {
        case GI_INFO_TYPE_INVALID: return GiInfoType_INVALID;
        case GI_INFO_TYPE_FUNCTION: return GiInfoType_FUNCTION;
        case GI_INFO_TYPE_CALLBACK: return GiInfoType_CALLBACK;
        case GI_INFO_TYPE_STRUCT: return GiInfoType_STRUCT;
        case GI_INFO_TYPE_BOXED: return GiInfoType_BOXED;
        case GI_INFO_TYPE_ENUM: return GiInfoType_ENUM;
        case GI_INFO_TYPE_FLAGS: return GiInfoType_FLAGS;
        case GI_INFO_TYPE_OBJECT: return GiInfoType_OBJECT;
        case GI_INFO_TYPE_INTERFACE: return GiInfoType_INTERFACE;
        case GI_INFO_TYPE_CONSTANT: return GiInfoType_CONSTANT;
        case GI_INFO_TYPE_INVALID_0: return GiInfoType_INVALID_0;
        case GI_INFO_TYPE_UNION: return GiInfoType_UNION;
        case GI_INFO_TYPE_VALUE: return GiInfoType_VALUE;
        case GI_INFO_TYPE_SIGNAL: return GiInfoType_SIGNAL;
        case GI_INFO_TYPE_VFUNC: return GiInfoType_VFUNC;
        case GI_INFO_TYPE_PROPERTY: return GiInfoType_PROPERTY;
        case GI_INFO_TYPE_FIELD: return GiInfoType_FIELD;
        case GI_INFO_TYPE_ARG: return GiInfoType_ARG;
        case GI_INFO_TYPE_TYPE: return GiInfoType_TYPE;
        default: return nullptr;
    }
}

jobject convertGiFunctionInfoFlags(JNIEnv* env, GIFunctionInfoFlags input) {
    //printf("FLAGS: %d\n", input);
    return populateSet(env, [&](auto add) {
        if(input & GI_FUNCTION_IS_METHOD) add(GiFunctionInfoFlags_IS_METHOD);
        if(input & GI_FUNCTION_IS_CONSTRUCTOR) add(GiFunctionInfoFlags_IS_CONSTRUCTOR);
        if(input & GI_FUNCTION_IS_GETTER) add(GiFunctionInfoFlags_IS_GETTER);
        if(input & GI_FUNCTION_IS_SETTER) add(GiFunctionInfoFlags_IS_SETTER);
        if(input & GI_FUNCTION_WRAPS_VFUNC) add(GiFunctionInfoFlags_WRAPS_VFUNC);
        if(input & GI_FUNCTION_THROWS) add(GiFunctionInfoFlags_THROWS);
    });
}

GIInfoType convertGiInfoType(JNIEnv* env, jobject input) {
    if(env->IsSameObject(input, GiInfoType_INVALID)) return GI_INFO_TYPE_INVALID;
    if(env->IsSameObject(input, GiInfoType_FUNCTION)) return GI_INFO_TYPE_FUNCTION;
    if(env->IsSameObject(input, GiInfoType_CALLBACK)) return GI_INFO_TYPE_CALLBACK;
    if(env->IsSameObject(input, GiInfoType_STRUCT)) return GI_INFO_TYPE_STRUCT;
    if(env->IsSameObject(input, GiInfoType_BOXED)) return GI_INFO_TYPE_BOXED;
    if(env->IsSameObject(input, GiInfoType_ENUM)) return GI_INFO_TYPE_ENUM;
    if(env->IsSameObject(input, GiInfoType_FLAGS)) return GI_INFO_TYPE_FLAGS;
    if(env->IsSameObject(input, GiInfoType_OBJECT)) return GI_INFO_TYPE_OBJECT;
    if(env->IsSameObject(input, GiInfoType_INTERFACE)) return GI_INFO_TYPE_INTERFACE;
    if(env->IsSameObject(input, GiInfoType_CONSTANT)) return GI_INFO_TYPE_CONSTANT;
    if(env->IsSameObject(input, GiInfoType_INVALID_0)) return GI_INFO_TYPE_INVALID_0;
    if(env->IsSameObject(input, GiInfoType_UNION)) return GI_INFO_TYPE_UNION;
    if(env->IsSameObject(input, GiInfoType_VALUE)) return GI_INFO_TYPE_VALUE;
    if(env->IsSameObject(input, GiInfoType_SIGNAL)) return GI_INFO_TYPE_SIGNAL;
    if(env->IsSameObject(input, GiInfoType_VFUNC)) return GI_INFO_TYPE_VFUNC;
    if(env->IsSameObject(input, GiInfoType_PROPERTY)) return GI_INFO_TYPE_PROPERTY;
    if(env->IsSameObject(input, GiInfoType_FIELD)) return GI_INFO_TYPE_FIELD;
    if(env->IsSameObject(input, GiInfoType_ARG)) return GI_INFO_TYPE_ARG;
    if(env->IsSameObject(input, GiInfoType_TYPE)) return GI_INFO_TYPE_TYPE;
    return GI_INFO_TYPE_UNRESOLVED;
}

jobject convertGiTypeTag(JNIEnv* env, GITypeTag input) {
    switch(input) {
        case GI_TYPE_TAG_VOID: return GiTypeTag_VOID;
        case GI_TYPE_TAG_BOOLEAN: return GiTypeTag_BOOLEAN;
        case GI_TYPE_TAG_INT8: return GiTypeTag_INT8;
        case GI_TYPE_TAG_UINT8: return GiTypeTag_UINT8;
        case GI_TYPE_TAG_INT16: return GiTypeTag_INT16;
        case GI_TYPE_TAG_UINT16: return GiTypeTag_UINT16;
        case GI_TYPE_TAG_INT32: return GiTypeTag_INT32;
        case GI_TYPE_TAG_UINT32: return GiTypeTag_UINT32;
        case GI_TYPE_TAG_INT64: return GiTypeTag_INT64;
        case GI_TYPE_TAG_UINT64: return GiTypeTag_UINT64;
        case GI_TYPE_TAG_FLOAT: return GiTypeTag_FLOAT;
        case GI_TYPE_TAG_DOUBLE: return GiTypeTag_DOUBLE;
        case GI_TYPE_TAG_GTYPE: return GiTypeTag_GTYPE;
        case GI_TYPE_TAG_UTF8: return GiTypeTag_UTF8;
        case GI_TYPE_TAG_FILENAME: return GiTypeTag_FILENAME;
        case GI_TYPE_TAG_ARRAY: return GiTypeTag_ARRAY;
        case GI_TYPE_TAG_INTERFACE: return GiTypeTag_INTERFACE;
        case GI_TYPE_TAG_GLIST: return GiTypeTag_GLIST;
        case GI_TYPE_TAG_GSLIST: return GiTypeTag_GSLIST;
        case GI_TYPE_TAG_GHASH: return GiTypeTag_GHASH;
        case GI_TYPE_TAG_ERROR: return GiTypeTag_ERROR;
        case GI_TYPE_TAG_UNICHAR: return GiTypeTag_UNICHAR;
        default: return nullptr;
    }
}

GITypeTag convertGiTypeTag(JNIEnv* env, jobject input) {
    if(env->IsSameObject(input, GiTypeTag_VOID)) return GI_TYPE_TAG_VOID;
    if(env->IsSameObject(input, GiTypeTag_BOOLEAN)) return GI_TYPE_TAG_BOOLEAN;
    if(env->IsSameObject(input, GiTypeTag_INT8)) return GI_TYPE_TAG_INT8;
    if(env->IsSameObject(input, GiTypeTag_UINT8)) return GI_TYPE_TAG_UINT8;
    if(env->IsSameObject(input, GiTypeTag_INT16)) return GI_TYPE_TAG_INT16;
    if(env->IsSameObject(input, GiTypeTag_UINT16)) return GI_TYPE_TAG_UINT16;
    if(env->IsSameObject(input, GiTypeTag_INT32)) return GI_TYPE_TAG_INT32;
    if(env->IsSameObject(input, GiTypeTag_UINT32)) return GI_TYPE_TAG_UINT32;
    if(env->IsSameObject(input, GiTypeTag_INT64)) return GI_TYPE_TAG_INT64;
    if(env->IsSameObject(input, GiTypeTag_UINT64)) return GI_TYPE_TAG_UINT64;
    if(env->IsSameObject(input, GiTypeTag_FLOAT)) return GI_TYPE_TAG_FLOAT;
    if(env->IsSameObject(input, GiTypeTag_DOUBLE)) return GI_TYPE_TAG_DOUBLE;
    if(env->IsSameObject(input, GiTypeTag_GTYPE)) return GI_TYPE_TAG_GTYPE;
    if(env->IsSameObject(input, GiTypeTag_UTF8)) return GI_TYPE_TAG_UTF8;
    if(env->IsSameObject(input, GiTypeTag_FILENAME)) return GI_TYPE_TAG_FILENAME;
    if(env->IsSameObject(input, GiTypeTag_ARRAY)) return GI_TYPE_TAG_ARRAY;
    if(env->IsSameObject(input, GiTypeTag_INTERFACE)) return GI_TYPE_TAG_INTERFACE;
    if(env->IsSameObject(input, GiTypeTag_GLIST)) return GI_TYPE_TAG_GLIST;
    if(env->IsSameObject(input, GiTypeTag_GSLIST)) return GI_TYPE_TAG_GSLIST;
    if(env->IsSameObject(input, GiTypeTag_GHASH)) return GI_TYPE_TAG_GHASH;
    if(env->IsSameObject(input, GiTypeTag_ERROR)) return GI_TYPE_TAG_ERROR;
    if(env->IsSameObject(input, GiTypeTag_UNICHAR)) return GI_TYPE_TAG_UNICHAR;
}
