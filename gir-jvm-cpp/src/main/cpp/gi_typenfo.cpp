#include "gi.h"

GITypeInfo* getTypeInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GITypeInfo>(env, thiz, GiTypeInfo_ptr);
}

JNIEXPORT jboolean JNICALL GiTypeInfo_isPointer(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    return g_type_info_is_pointer(info);
}

JNIEXPORT jobject JNICALL GiTypeInfo_getTag(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    auto result = g_type_info_get_tag(info);
    return convertGiTypeTag(env, result);
}
JNIEXPORT jobject JNICALL GiTypeInfo_paramType(JNIEnv* env, jobject thiz, jint num) {
    auto info = getTypeInfo(env, thiz);
    auto result = g_type_info_get_param_type(info, num);
    return constructObjectWithNativePtr(env, GiTypeInfo, GiTypeInfo_ctor, result);
}
JNIEXPORT jobject JNICALL GiTypeInfo_getIface(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    auto result = g_type_info_get_interface(info);
    return constructObjectWithNativePtr(env, GiBaseInfo, GiBaseInfo_ctor, result);
}

JNIEXPORT jint JNICALL GiTypeInfo_getArrayLength(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    return g_type_info_get_array_length(info);
}

JNIEXPORT jint JNICALL GiTypeInfo_getArrayFixedSize(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    return g_type_info_get_array_fixed_size(info);
}

JNIEXPORT jboolean JNICALL GiTypeInfo_isZeroTerminated(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    return g_type_info_is_zero_terminated(info);
}

JNIEXPORT jobject JNICALL GiTypeInfo_getArrayType(JNIEnv* env, jobject thiz) {
    auto info = getTypeInfo(env, thiz);
    auto result = g_type_info_get_array_type(info);
    return convertGiArrayType(env, result);
}
