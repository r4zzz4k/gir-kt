#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wwrite-strings"

static char const* GiRepositoryKt_className = "me/r4zzz4k/girkt/gir/GiRepositoryKt";
static JNINativeMethod GiRepositoryKt_methods[] = {
    { "giRepository", "()Lme/r4zzz4k/girkt/gir/GiRepository;", (void*)GiRepositoryKt_giRepository },
};

static char const* EnumsKt_className = "me/r4zzz4k/girkt/gir/EnumsKt";
static JNINativeMethod EnumsKt_methods[] = {
    { "giInfoTypeToString", "(Lme/r4zzz4k/girkt/gir/GiInfoType;)Ljava/lang/String;", (void*)EnumsKt_giInfoTypeToString },
    { "giTypeTagToString", "(Lme/r4zzz4k/girkt/gir/GiTypeTag;)Ljava/lang/String;", (void*)EnumsKt_giTypeTagToString },
};

static char const* GiArgInfo_className = "me/r4zzz4k/girkt/gir/GiArgInfo";
static JNINativeMethod GiArgInfo_methods[] = {
    { "getClosure", "()I", (void*)GiArgInfo_getClosure },
    { "getDestroy", "()I", (void*)GiArgInfo_getDestroy },
    { "getDirection", "()Lme/r4zzz4k/girkt/gir/GiDirection;", (void*)GiArgInfo_getDirection },
    { "getOwnershipTransfer", "()Lme/r4zzz4k/girkt/gir/GiTransfer;", (void*)GiArgInfo_getOwnershipTransfer },
    { "getScope", "()Lme/r4zzz4k/girkt/gir/GiScopeType;", (void*)GiArgInfo_getScope },
    { "getType", "()Lme/r4zzz4k/girkt/gir/GiTypeInfo;", (void*)GiArgInfo_getType },
    { "getNullable", "()Z", (void*)GiArgInfo_getNullable },
    { "getCallerAllocates", "()Z", (void*)GiArgInfo_getCallerAllocates },
    { "getOptional", "()Z", (void*)GiArgInfo_getOptional },
    { "isReturnValue", "()Z", (void*)GiArgInfo_isReturnValue },
    { "getSkip", "()Z", (void*)GiArgInfo_getSkip },
};

static char const* GiBaseInfo_className = "me/r4zzz4k/girkt/gir/GiBaseInfo";
static JNINativeMethod GiBaseInfo_methods[] = {
    { "getInfoType", "()Lme/r4zzz4k/girkt/gir/GiInfoType;", (void*)GiBaseInfo_getInfoType },
    { "getNamespace", "()Ljava/lang/String;", (void*)GiBaseInfo_getNamespace },
    { "getName", "()Ljava/lang/String;", (void*)GiBaseInfo_getName },
    { "getAttributes", "()Ljava/util/Map;", (void*)GiBaseInfo_getAttributes },
};

static char const* GiCallableInfo_className = "me/r4zzz4k/girkt/gir/GiCallableInfo";
static JNINativeMethod GiCallableInfo_methods[] = {
    { "getThrows", "()Z", (void*)GiCallableInfo_getThrows },
    { "getArgs", "()Ljava/util/List;", (void*)GiCallableInfo_getArgs },
    { "getCallerOwns", "()Lme/r4zzz4k/girkt/gir/GiTransfer;", (void*)GiCallableInfo_getCallerOwns },
    { "getReturnAttributes", "()Ljava/util/Map;", (void*)GiCallableInfo_getReturnAttributes },
    { "getReturnType", "()Lme/r4zzz4k/girkt/gir/GiTypeInfo;", (void*)GiCallableInfo_getReturnType },
    { "isMethod", "()Z", (void*)GiCallableInfo_getIsMethod },
    { "getReturnNullable", "()Z", (void*)GiCallableInfo_getReturnNullable },
};

static char const* GiCallbackInfo_className = "me/r4zzz4k/girkt/gir/GiCallbackInfo";
static JNINativeMethod GiCallbackInfo_methods[] = {
};

static char const* GiConstantInfo_className = "me/r4zzz4k/girkt/gir/GiConstantInfo";
static JNINativeMethod GiConstantInfo_methods[] = {
    { "getType", "()Lme/r4zzz4k/girkt/gir/GiTypeInfo;", (void*)GiConstantInfo_getType },
    { "getValue", "()Lme/r4zzz4k/girkt/gir/GiArgument;", (void*)GiConstantInfo_getValue },
    { "freeValue", "(Lme/r4zzz4k/girkt/gir/GiArgument;)", (void*)GiConstantInfo_freeValue },
};

static char const* GiEnumInfo_className = "me/r4zzz4k/girkt/gir/GiEnumInfo";
static JNINativeMethod GiEnumInfo_methods[] = {
    { "getValues", "()Ljava/util/List;", (void*)GiEnumInfo_getValues },
    { "getMethods", "()Ljava/util/List;", (void*)GiEnumInfo_getMethods },
    { "getStorageType", "()Lme/r4zzz4k/girkt/gir/GiTypeTag;", (void*)GiEnumInfo_getStorageType },
    { "getErrorDomain", "()Ljava/lang/String;", (void*)GiEnumInfo_getErrorDomain },
};

static char const* GiFieldInfo_className = "me/r4zzz4k/girkt/gir/GiFieldInfo";
static JNINativeMethod GiFieldInfo_methods[] = {
    { "getFlags", "()Ljava/util/List;", (void*)GiFieldInfo_getFlags },
};

static char const* GiFunctionInfo_className = "me/r4zzz4k/girkt/gir/GiFunctionInfo";
static JNINativeMethod GiFunctionInfo_methods[] = {
    { "getFlags", "()Ljava/util/Set;", (void*)GiFunctionInfo_getFlags },
    { "getProperty", "()Lme/r4zzz4k/girkt/gir/GiPropertyInfo;", (void*)GiFunctionInfo_getProperty },
    { "getSymbol", "()Ljava/lang/String;", (void*)GiFunctionInfo_getSymbol },
    { "getVfunc", "()Lme/r4zzz4k/girkt/gir/GiVFuncInfo;", (void*)GiFunctionInfo_getVfunc },
    { "invoke", "([Ljava/lang/Object;)Ljava/lang/Object;", (void*)GiFunctionInfo_invoke },
};//varargs invoke([Ljava/lang/Object;)Ljava/lang/Object;

static char const* GiInterfaceInfo_className = "me/r4zzz4k/girkt/gir/GiInterfaceInfo";
static JNINativeMethod GiInterfaceInfo_methods[] = {
    { "getPrerequisites", "()Ljava/util/List;", (void*)GiInterfaceInfo_getPrerequisites },
    { "getProperties", "()Ljava/util/List;", (void*)GiInterfaceInfo_getProperties },
    { "getMethods", "()Ljava/util/List;", (void*)GiInterfaceInfo_getMethods },
    { "findMethod", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiFunctionInfo;", (void*)GiInterfaceInfo_findMethod },
    { "getSignals", "()Ljava/util/List;", (void*)GiInterfaceInfo_getSignals },
    { "findSignal", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiSignalInfo;", (void*)GiInterfaceInfo_findSignal },
    { "getVfuncs", "()Ljava/util/List;", (void*)GiInterfaceInfo_getVfuncs },
    { "findVfunc", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiVFuncInfo;", (void*)GiInterfaceInfo_findVfunc },
    { "getConstants", "()Ljava/util/List;", (void*)GiInterfaceInfo_getConstants },
    { "getIfaceStruct", "()Lme/r4zzz4k/girkt/gir/GiStructInfo;", (void*)GiInterfaceInfo_getIfaceStruct },
};

static char const* GiObjectInfo_className = "me/r4zzz4k/girkt/gir/GiObjectInfo";
static JNINativeMethod GiObjectInfo_methods[] = {
    { "getAbstract", "()Z", (void*)GiObjectInfo_getAbstract },
    { "getFundamental", "()Z", (void*)GiObjectInfo_getFundamental },
    { "getParent", "()Lme/r4zzz4k/girkt/gir/GiObjectInfo;", (void*)GiObjectInfo_getParent },
    { "getTypeName", "()Ljava/lang/String;", (void*)GiObjectInfo_getTypeName },
    { "getTypeInit", "()Ljava/lang/String;", (void*)GiObjectInfo_getTypeInit },
    { "getConstants", "()Ljava/util/List;", (void*)GiObjectInfo_getConstants },
    { "getFields", "()Ljava/util/List;", (void*)GiObjectInfo_getFields },
    { "getInterfaces", "()Ljava/util/List;", (void*)GiObjectInfo_getInterfaces },
    { "getMethods", "()Ljava/util/List;", (void*)GiObjectInfo_getMethods },
    { "findMethod", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiFunctionInfo;", (void*)GiObjectInfo_findMethod },
    { "getProperties", "()Ljava/util/List;", (void*)GiObjectInfo_getProperties },
    { "getSignals", "()Ljava/util/List;", (void*)GiObjectInfo_getSignals },
    { "findSignal", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiSignalInfo;", (void*)GiObjectInfo_findSignal },
    { "getVfuncs", "()Ljava/util/List;", (void*)GiObjectInfo_getVfuncs },
    { "findVfunc", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiVFuncInfo;", (void*)GiObjectInfo_findVfunc },
    { "getClassStruct", "()Lme/r4zzz4k/girkt/gir/GiStructInfo;", (void*)GiObjectInfo_getClassStruct },
};

static char const* GiPropertyInfo_className = "me/r4zzz4k/girkt/gir/GiPropertyInfo";
static JNINativeMethod GiPropertyInfo_methods[] = {
    //{ "", "()", (void*)GiPropertyInfo_ },
};

static char const* GiRegisteredTypeInfo_className = "me/r4zzz4k/girkt/gir/GiRegisteredTypeInfo";
static JNINativeMethod GiRegisteredTypeInfo_methods[] = {
    { "getTypeName", "()Ljava/lang/String;", (void*)GiRegisteredTypeInfo_getTypeName },
    { "getTypeInit", "()Ljava/lang/String;", (void*)GiRegisteredTypeInfo_getTypeInit },
    { "getGType", "()Lme/r4zzz4k/girkt/gir/GTypeRef;", (void*)GiRegisteredTypeInfo_getGType },
};

static char const* GiRepository_className = "me/r4zzz4k/girkt/gir/GiRepository";
static JNINativeMethod GiRepository_methods[] = {
    { "require", "(Ljava/lang/String;Ljava/lang/String;Z)V", (void*)GiRepository_require },
    { "getSharedLibraries", "(Ljava/lang/String;)Ljava/util/List;", (void*)GiRepository_getSharedLibraries },
    { "getInfos", "(Ljava/lang/String;)Ljava/util/List;", (void*)GiRepository_getInfos },
};

static char const* GiSignalInfo_className = "me/r4zzz4k/girkt/gir/GiSignalInfo";
static JNINativeMethod GiSignalInfo_methods[] = {
    { "getFlags", "()Lme/r4zzz4k/girkt/gir/GSignalFlags;", (void*)GiSignalInfo_getFlags },
    { "getClassClosure", "()Lme/r4zzz4k/girkt/gir/GiVFuncInfo;", (void*)GiSignalInfo_getClassClosure },
    { "getTrueStopsEmit", "()Z", (void*)GiSignalInfo_getTrueStopsEmit },
};

static char const* GiStructInfo_className = "me/r4zzz4k/girkt/gir/GiStructInfo";
static JNINativeMethod GiStructInfo_methods[] = {
    { "getAlignment", "()I", (void*)GiStructInfo_getAlignment },
    { "getSize", "()I", (void*)GiStructInfo_getSize },
    { "isGtypeStruct", "()Z", (void*)GiStructInfo_isGtypeStruct },
    { "getForeign", "()Z", (void*)GiStructInfo_getForeign },
    { "getFields", "()Ljava/util/List;", (void*)GiStructInfo_getFields },
    { "getMethods", "()Ljava/util/List;", (void*)GiStructInfo_getMethods },
    { "findMethod", "(Ljava/lang/String;)Lme/r4zzz4k/girkt/gir/GiFunctionInfo;", (void*)GiStructInfo_findMethod },
};

static char const* GiTypeInfo_className = "me/r4zzz4k/girkt/gir/GiTypeInfo";
static JNINativeMethod GiTypeInfo_methods[] = {
    { "isPointer", "()Z", (void*)GiTypeInfo_isPointer },
    { "getTag", "()Lme/r4zzz4k/girkt/gir/GiTypeTag;", (void*)GiTypeInfo_getTag },
    { "paramType", "(I)Lme/r4zzz4k/girkt/gir/GiTypeInfo;", (void*)GiTypeInfo_paramType },
    { "getIface", "()Lme/r4zzz4k/girkt/gir/GiBaseInfo;", (void*)GiTypeInfo_getIface },
    { "getArrayLength", "()I", (void*)GiTypeInfo_getArrayLength },
    { "getArrayFixedSize", "()I", (void*)GiTypeInfo_getArrayFixedSize },
    { "isZeroTerminated", "()Z", (void*)GiTypeInfo_isZeroTerminated },
    { "getArrayType", "()Lme/r4zzz4k/girkt/gir/GiArrayType;", (void*)GiTypeInfo_getArrayType },
};

static char const* GiUnionInfo_className = "me/r4zzz4k/girkt/gir/GiUnionInfo";
static JNINativeMethod GiUnionInfo_methods[] = {
    //{ "", "()Lme/r4zzz4k/girkt/gir/;", (void*)GiUnionInfo_ },
};

static char const* GiVFuncInfo_className = "me/r4zzz4k/girkt/gir/GiVFuncInfo";
static JNINativeMethod GiVFuncInfo_methods[] = {
    { "getFlags", "()Lme/r4zzz4k/girkt/gir/GiVFuncInfoFlags;", (void*)GiVFuncInfo_getFlags },
    { "getOffset", "()I", (void*)GiVFuncInfo_getOffset },
    { "getSignal", "()Lme/r4zzz4k/girkt/gir/GiSignalInfo;", (void*)GiVFuncInfo_getSignal },
    { "getInvoker", "()Lme/r4zzz4k/girkt/gir/GiFunctionInfo;", (void*)GiVFuncInfo_getInvoker },
};

static char const* GiValueInfo_className = "me/r4zzz4k/girkt/gir/GiValueInfo";
static JNINativeMethod GiValueInfo_methods[] = {
    { "getValue", "()J", (void*)GiValueInfo_getValue },
};

bool registerNativeClasses(JNIEnv* env) {
    return
        registerNativeClass(env, GiRepositoryKt) &&
        registerNativeClass(env, EnumsKt) &&
        registerNativeClass(env, GiArgInfo) &&
        registerNativeClass(env, GiBaseInfo) &&
        registerNativeClass(env, GiCallableInfo) &&
        registerNativeClass(env, GiCallbackInfo) &&
        registerNativeClass(env, GiEnumInfo) &&
        registerNativeClass(env, GiFunctionInfo) &&
        registerNativeClass(env, GiObjectInfo) &&
        registerNativeClass(env, GiPropertyInfo) &&
        registerNativeClass(env, GiRegisteredTypeInfo) &&
        registerNativeClass(env, GiRepository) &&
        registerNativeClass(env, GiSignalInfo) &&
        registerNativeClass(env, GiStructInfo) &&
        registerNativeClass(env, GiTypeInfo) &&
        registerNativeClass(env, GiVFuncInfo) &&
        registerNativeClass(env, GiValueInfo) &&
        true;
}

#pragma clang diagnostic pop
