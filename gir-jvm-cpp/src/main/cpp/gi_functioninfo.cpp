#include "gi.h"
#include <list>
#include <utility>
#include <vector>

GIFunctionInfo* getFunctionInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIFunctionInfo>(env, thiz, GiFunctionInfo_ptr);
}

JNIEXPORT jobject JNICALL GiFunctionInfo_getFlags(JNIEnv* env, jobject thiz) {
    auto info = getFunctionInfo(env, thiz);
    auto result = g_function_info_get_flags(info);
    return convertGiFunctionInfoFlags(env, result);
}

JNIEXPORT jobject JNICALL GiFunctionInfo_getProperty(JNIEnv* env, jobject thiz) {
    auto info = getFunctionInfo(env, thiz);
    auto result = g_function_info_get_property(info);
    return constructObjectWithNativePtr(env, GiPropertyInfo, GiPropertyInfo_ctor, result);
}

JNIEXPORT jstring JNICALL GiFunctionInfo_getSymbol(JNIEnv* env, jobject thiz) {
    auto info = getFunctionInfo(env, thiz);
    auto resultPtr = g_function_info_get_symbol(info);
    return env->NewStringUTF(resultPtr);
}

JNIEXPORT jobject JNICALL GiFunctionInfo_getVfunc(JNIEnv* env, jobject thiz) {
    auto info = getFunctionInfo(env, thiz);
    auto result = g_function_info_get_vfunc(info);
    return constructObjectWithNativePtr(env, GiVFuncInfo, GiVFuncInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiFunctionInfo_invoke(JNIEnv* env, jobject thiz, jobjectArray args /* TODO */) {
    auto info = getFunctionInfo(env, thiz);
    fprintf(stderr, "INVOKING %s\n", g_function_info_get_symbol(info));

    std::list<std::pair<jstring, char const*>> toRelease;

    auto actualArgCount = env->GetArrayLength(args);
    int formalArgCount = g_callable_info_get_n_args(info);
    auto actualOffset = 0;

    std::vector<GIArgument> inArgs;
    std::vector<GIArgument> outArgs;
    auto returnValue = (GIArgument*)malloc(sizeof(GIArgument));
    GIArgument arg;

    if(!(g_function_info_get_flags(info) & GI_FUNCTION_IS_CONSTRUCTOR)) {
        actualOffset = 1;

        auto receiver = (jobject) env->GetObjectArrayElement(args, 0);
        if(!env->IsInstanceOf(receiver, GObjectRef)) {
            fprintf(stderr, "First argument should be GObjectRef\n");
            return nullptr;
        }

        arg.v_pointer = getNativePtr<GObject>(env, receiver, GObjectRef_ptr);
        inArgs.push_back(arg);

        // TODO get first element frm args, check if it's GObjectRef, put into inArgs:
        // https://github.com/creationix/node-gir/blob/da0b211c1d0b2243edb9cb86b293706e0b94ee14/src/function.cc#L106
    }

    if(formalArgCount != actualArgCount - actualOffset) {
        fprintf(stderr, "Parameter count mismatch! Formal: %d, actual: %d\n",
                formalArgCount,
                actualArgCount - actualOffset);
    }

    for(auto i = 0; i < formalArgCount; ++i) {
        auto argInfo = g_callable_info_get_arg(info, i);
        auto dir = g_arg_info_get_direction(argInfo);

        if(dir == GI_DIRECTION_OUT || dir == GI_DIRECTION_INOUT) {
            fprintf(stderr, "* Out arg: %s (%s)\n",
                    g_base_info_get_name(argInfo),
                    g_type_tag_to_string(g_type_info_get_tag(g_arg_info_get_type(argInfo))));
            arg.v_string = nullptr;
            outArgs.push_back(arg);
        }
        if(dir == GI_DIRECTION_IN || dir == GI_DIRECTION_INOUT) {
            fprintf(stderr, "* In arg: %s (%s)\n",
                    g_base_info_get_name(argInfo),
                    g_type_tag_to_string(g_type_info_get_tag(g_arg_info_get_type(argInfo))));
            arg.v_string = nullptr;

            if(!convertGiArgument(
                    env,
                    (jobject) env->GetObjectArrayElement(args, i + actualOffset),
                    &arg,
                    g_arg_info_get_type(argInfo),
                    g_arg_info_may_be_null(argInfo),
                    g_base_info_get_name(argInfo))) {
                fprintf(stderr, "Unable to call function\n");
                // TODO raise exception
                return nullptr;
            }

            inArgs.push_back(arg);
        }
    }

    /*auto ptr = &inArgsArray[0];
    listForEach(env, inArgs, [&](jobject element) {
        if(env->IsInstanceOf(element, Ref)) {
            auto valueRef = env->GetObjectField(element, Ref_ref);
            if(env->IsInstanceOf(valueRef, String)) {
                auto valuePtr = (void*) env->GetStringUTFChars((jstring) valueRef, nullptr);
                toRelease.emplace_back(std::make_pair((jstring) valueRef, (char const*) valuePtr));
                ptr->v_pointer = valuePtr;
            } else {
                fputs("Unprocessed reference type: ", stderr);
                printClassName(stderr, env, element);
                fputs("\n", stderr);
            }
            // ptr->vPointer = ...
        } else if(env->IsInstanceOf(element, Integer)) {
            auto value = env->CallIntMethod(element, Integer_intValue);
            ptr->v_int = value;
        } else {
            fputs("Unprocessed type: ", stderr);
            printClassName(stderr, env, element);
            fputs("\n", stderr);
        }

        ++ptr;
    });*/

    GError* error = nullptr;

    fprintf(stderr, "Total in: %lu, out: %lu\n", inArgs.size(), outArgs.size());
    //fprintf(stderr, "GType: %s, count: %d, first prop: [%s] [%s]\n",
    //        g_type_name(inArgs[0].v_long),
    //        inArgs[1].v_int,
    //        ((char**) inArgs[2].v_pointer)[0],
    //        ((char**) inArgs[3].v_pointer)[0]);



    //fprintf(stderr, "### Type is value: %s\n", G_TYPE_IS_VALUE(G_VALUE_TYPE(&(((char**) inArgs[3].v_pointer)[0])[0])));

    if(!g_function_info_invoke(
            info,
            inArgs.data(),
            inArgs.size(),
            outArgs.data(),
            outArgs.size(),
            returnValue,
            &error)) {
        // TODO process error
        fprintf(stderr, "Unable to invoke function: %s\n", error->message);
    } else {
        fprintf(stderr, "Function invoked successfully\n");
    }

    for(auto& pair: toRelease) {
        env->ReleaseStringUTFChars(pair.first, pair.second);
    }

    return convertGiArgument(env, returnValue, g_callable_info_get_return_type(info), "[return value]");

    /*fprintf(stderr, "Creating GiArgument\n");
    auto rrr = constructObjectWithNativePtr(env, GiArgument, GiArgument_ctor, returnValue);
    fprintf(stderr, "DONE\n");
    return rrr;*/
}
