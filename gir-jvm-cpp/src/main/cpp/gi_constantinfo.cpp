#include "gi.h"

GIConstantInfo* getConstantInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIConstantInfo>(env, thiz, GiConstantInfo_ptr);
}

JNIEXPORT jobject JNICALL GiConstantInfo_getType(JNIEnv* env, jobject thiz) {
    auto info = getConstantInfo(env, thiz);
    auto result = g_constant_info_get_type(info);
    return constructObjectWithNativePtr(env, GiTypeInfo, GiTypeInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiConstantInfo_getValue(JNIEnv* env, jobject thiz) {
    auto info = getConstantInfo(env, thiz);
    auto value = (GIArgument*)malloc(sizeof(GIArgument));
    auto result = g_constant_info_get_value(info, value);
    return constructObjectWithNativePtr(env, GiArgument, GiArgument_ctor, value);
}

JNIEXPORT void JNICALL GiConstantInfo_freeValue(JNIEnv* env, jobject thiz, jobject value) {
    auto info = getConstantInfo(env, thiz);
    auto ptr = getNativePtr<GIArgument>(env, value, GiArgument_ptr);
    g_constant_info_free_value(info, ptr);
    free(ptr);
}
