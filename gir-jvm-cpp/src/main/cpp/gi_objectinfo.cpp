#include "gi.h"

GIObjectInfo* getObjectInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIObjectInfo>(env, thiz, GiObjectInfo_ptr);
}

JNIEXPORT jboolean JNICALL GiObjectInfo_getAbstract(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return g_object_info_get_abstract(info);
}

JNIEXPORT jboolean JNICALL GiObjectInfo_getFundamental(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return g_object_info_get_fundamental(info);
}

JNIEXPORT jobject JNICALL GiObjectInfo_getParent(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    auto result = g_object_info_get_parent(info);
    return constructObjectWithNativePtr(env, GiObjectInfo, GiObjectInfo_ctor, result);
}

JNIEXPORT jstring JNICALL GiObjectInfo_getTypeName(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    auto result = g_object_info_get_type_name(info);
    return env->NewStringUTF(result);
}


JNIEXPORT jstring JNICALL GiObjectInfo_getTypeInit(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    auto result = g_object_info_get_type_init(info);
    return env->NewStringUTF(result);
}

JNIEXPORT jobject JNICALL GiObjectInfo_getConstants(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_constants(info); ++i) {
            auto result = g_object_info_get_constant(info, i);
            add(constructObjectWithNativePtr(env, GiConstantInfo, GiConstantInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_getFields(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_fields(info); ++i) {
            auto result = g_object_info_get_field(info, i);
            add(constructObjectWithNativePtr(env, GiFieldInfo, GiFieldInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_getInterfaces(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_interfaces(info); ++i) {
            auto result = g_object_info_get_interface(info, i);
            add(constructObjectWithNativePtr(env, GiInterfaceInfo, GiInterfaceInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_getMethods(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_methods(info); ++i) {
            auto result = g_object_info_get_method(info, i);
            add(constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_findMethod(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getObjectInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_object_info_find_method(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiObjectInfo_getProperties(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_properties(info); ++i) {
            auto result = g_object_info_get_property(info, i);
            add(constructObjectWithNativePtr(env, GiPropertyInfo, GiPropertyInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_getSignals(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_signals(info); ++i) {
            auto result = g_object_info_get_signal(info, i);
            add(constructObjectWithNativePtr(env, GiSignalInfo, GiSignalInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_findSignal(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getObjectInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_object_info_find_signal(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiSignalInfo, GiSignalInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiObjectInfo_getVfuncs(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_object_info_get_n_vfuncs(info); ++i) {
            auto result = g_object_info_get_vfunc(info, i);
            add(constructObjectWithNativePtr(env, GiVFuncInfo, GiVFuncInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiObjectInfo_findVfunc(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getObjectInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_object_info_find_vfunc(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiVFuncInfo, GiVFuncInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiObjectInfo_getClassStruct(JNIEnv* env, jobject thiz) {
    auto info = getObjectInfo(env, thiz);
    auto result = g_object_info_get_class_struct(info);
    return constructObjectWithNativePtr(env, GiStructInfo, GiStructInfo_ctor, result);
}
