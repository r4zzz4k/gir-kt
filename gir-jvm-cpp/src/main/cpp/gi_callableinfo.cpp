#include "gi.h"

GICallableInfo* getCallableInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GICallableInfo>(env, thiz, GiCallableInfo_ptr);
}

JNIEXPORT jboolean JNICALL GiCallableInfo_getThrows(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    return g_callable_info_can_throw_gerror(info);
}

JNIEXPORT jobject JNICALL GiCallableInfo_getArgs(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_callable_info_get_n_args(info); ++i) {
            auto result = g_callable_info_get_arg(info, i);
            add(constructObjectWithNativePtr(env, GiArgInfo, GiArgInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiCallableInfo_getCallerOwns(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    auto result = g_callable_info_get_caller_owns(info);
    return convertGiTransfer(env, result);
}

JNIEXPORT jobject JNICALL GiCallableInfo_getReturnAttributes(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    return populateMap(env, [&](auto put) {
        GIAttributeIter iter = { 0, };
        char* name;
        char* value;
        while (g_callable_info_iterate_return_attributes(info, &iter, &name, &value)) {
            put(env->NewStringUTF(name), env->NewStringUTF(value));
        }
    });
}

JNIEXPORT jobject JNICALL GiCallableInfo_getReturnType(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    auto result = g_callable_info_get_return_type(info);
    return constructObjectWithNativePtr(env, GiTypeInfo, GiTypeInfo_ctor, result);
}

JNIEXPORT jboolean JNICALL GiCallableInfo_getIsMethod(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    //bool success = g_callable_info_invoke(info, ...);
    // TODO
    return false;
}

JNIEXPORT jboolean JNICALL GiCallableInfo_getReturnNullable(JNIEnv* env, jobject thiz) {
    auto info = getCallableInfo(env, thiz);
    return g_callable_info_may_return_null(info);
}
