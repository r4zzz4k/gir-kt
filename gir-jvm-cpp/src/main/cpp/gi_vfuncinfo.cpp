#include "gi.h"

GIVFuncInfo* getVFuncInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIVFuncInfo>(env, thiz, GiVFuncInfo_ptr);
}

JNIEXPORT jobject JNICALL GiVFuncInfo_getFlags(JNIEnv* env, jobject thiz) {
    auto info = getVFuncInfo(env, thiz);
    auto result = g_vfunc_info_get_flags(info);
    return convertGiVFuncInfoFlags(env, result);
}

JNIEXPORT jint JNICALL GiVFuncInfo_getOffset(JNIEnv* env, jobject thiz) {
    auto info = getVFuncInfo(env, thiz);
    return g_vfunc_info_get_offset(info);
}

JNIEXPORT jobject JNICALL GiVFuncInfo_getSignal(JNIEnv* env, jobject thiz) {
    auto info = getVFuncInfo(env, thiz);
    auto result = g_vfunc_info_get_signal(info);
    return constructObjectWithNativePtr(env, GiSignalInfo, GiSignalInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiVFuncInfo_getInvoker(JNIEnv* env, jobject thiz) {
    auto info = getVFuncInfo(env, thiz);
    auto result = g_vfunc_info_get_invoker(info);
    return constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result);
}
