#include "gi.h"

GIRegisteredTypeInfo* getRegisteredTypeInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIRegisteredTypeInfo>(env, thiz, GiRegisteredTypeInfo_ptr);
}

JNIEXPORT jstring JNICALL GiRegisteredTypeInfo_getTypeName(JNIEnv* env, jobject thiz) {
    auto info = getRegisteredTypeInfo(env, thiz);
    auto result = g_registered_type_info_get_type_name(info);
    return env->NewStringUTF(result);
}

JNIEXPORT jstring JNICALL GiRegisteredTypeInfo_getTypeInit(JNIEnv* env, jobject thiz) {
    auto info = getRegisteredTypeInfo(env, thiz);
    auto result = g_registered_type_info_get_type_init(info);
    return env->NewStringUTF(result);
}

JNIEXPORT jobject JNICALL GiRegisteredTypeInfo_getGType(JNIEnv* env, jobject thiz) {
    auto info = getRegisteredTypeInfo(env, thiz);
    auto result = g_registered_type_info_get_g_type(info);
    return constructObjectWithNativeLong(env, GTypeRef, GTypeRef_ctor, result);
}
