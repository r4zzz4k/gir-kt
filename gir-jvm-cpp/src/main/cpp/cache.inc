#define DEF_EXTERN
#include "cache_defs.inc"
#undef DEF_EXTERN

static jobject getEnumField(JNIEnv* env, jclass clazz, char const* name, char const* classNameType) {
    return globalize(env, env->GetStaticObjectField(clazz, env->GetStaticFieldID(clazz, name, classNameType)));
}

static void fillNativePtrClass(JNIEnv* env, char const* className, jclass& clazz, jmethodID& ctor, jfieldID& ptr) {
    clazz = globalize(env, env->FindClass(className));
    ctor = env->GetMethodID(clazz, "<init>", "(J)V");
    ptr = env->GetFieldID(clazz, "ptr", "J");
}
#define FILL_NATIVE_PTR_CLASS(env, shortName, fullName) \
        fillNativePtrClass(env, fullName, shortName, shortName ## _ctor, shortName ## _ptr)

bool fillCache(JNIEnv* env) {
    FILL_NATIVE_PTR_CLASS(env, GObjectRef, "me/r4zzz4k/girkt/gir/GObjectRef");
    FILL_NATIVE_PTR_CLASS(env, GPointer, "me/r4zzz4k/girkt/gir/GPointer");
    FILL_NATIVE_PTR_CLASS(env, GTypeRef, "me/r4zzz4k/girkt/gir/GTypeRef");
    FILL_NATIVE_PTR_CLASS(env, GiArgInfo, "me/r4zzz4k/girkt/gir/GiArgInfo");
    FILL_NATIVE_PTR_CLASS(env, GiArgument, "me/r4zzz4k/girkt/gir/GiArgument");
    FILL_NATIVE_PTR_CLASS(env, GiBaseInfo, "me/r4zzz4k/girkt/gir/GiBaseInfo");
    FILL_NATIVE_PTR_CLASS(env, GiCallableInfo, "me/r4zzz4k/girkt/gir/GiCallableInfo");
    FILL_NATIVE_PTR_CLASS(env, GiCallbackInfo, "me/r4zzz4k/girkt/gir/GiCallbackInfo");
    FILL_NATIVE_PTR_CLASS(env, GiConstantInfo, "me/r4zzz4k/girkt/gir/GiConstantInfo");
    FILL_NATIVE_PTR_CLASS(env, GiEnumInfo, "me/r4zzz4k/girkt/gir/GiEnumInfo");
    FILL_NATIVE_PTR_CLASS(env, GiFieldInfo, "me/r4zzz4k/girkt/gir/GiFieldInfo");
    FILL_NATIVE_PTR_CLASS(env, GiFunctionInfo, "me/r4zzz4k/girkt/gir/GiFunctionInfo");
    FILL_NATIVE_PTR_CLASS(env, GiInterfaceInfo, "me/r4zzz4k/girkt/gir/GiInterfaceInfo");
    FILL_NATIVE_PTR_CLASS(env, GiObjectInfo, "me/r4zzz4k/girkt/gir/GiObjectInfo");
    FILL_NATIVE_PTR_CLASS(env, GiPropertyInfo, "me/r4zzz4k/girkt/gir/GiPropertyInfo");
    FILL_NATIVE_PTR_CLASS(env, GiRegisteredTypeInfo, "me/r4zzz4k/girkt/gir/GiSignalInfo");
    FILL_NATIVE_PTR_CLASS(env, GiRepository, "me/r4zzz4k/girkt/gir/GiRepository");
    FILL_NATIVE_PTR_CLASS(env, GiSignalInfo, "me/r4zzz4k/girkt/gir/GiSignalInfo");
    FILL_NATIVE_PTR_CLASS(env, GiStructInfo, "me/r4zzz4k/girkt/gir/GiStructInfo");
    FILL_NATIVE_PTR_CLASS(env, GiTypeInfo, "me/r4zzz4k/girkt/gir/GiTypeInfo");
    FILL_NATIVE_PTR_CLASS(env, GiUnionInfo, "me/r4zzz4k/girkt/gir/GiUnionInfo");
    FILL_NATIVE_PTR_CLASS(env, GiVFuncInfo, "me/r4zzz4k/girkt/gir/GiVFuncInfo");
    FILL_NATIVE_PTR_CLASS(env, GiValueInfo, "me/r4zzz4k/girkt/gir/GiValueInfo");

    Array = globalize(env, env->FindClass("java/lang/reflect/Array"));
    Array_get = env->GetStaticMethodID(Array, "get", "(Ljava/lang/Object;I)Ljava/lang/Object;");
    Array_getLength = env->GetStaticMethodID(Array, "getLength", "(Ljava/lang/Object;)I");

    ArrayList = globalize(env, env->FindClass("java/util/ArrayList"));
    ArrayList_ctor = env->GetMethodID(ArrayList, "<init>", "()V");
    ArrayList_add = env->GetMethodID(ArrayList, "add", "(Ljava/lang/Object;)Z");

    Boolean = globalize(env, env->FindClass("java/lang/Boolean"));
    Boolean_booleanValue = env->GetMethodID(Boolean, "booleanValue", "()Z");
    Boolean_valueOf = env->GetStaticMethodID(Boolean, "valueOf", "(Z)Ljava/lang/Boolean;");

    Character = globalize(env, env->FindClass("java/lang/Character"));
    Character_charValue = env->GetMethodID(Character, "charValue", "()C");
    Character_valueOf = env->GetStaticMethodID(Character, "valueOf", "(C)Ljava/lang/Character;");

    Class = globalize(env, env->FindClass("java/lang/Class"));
    Class_getName = env->GetMethodID(Class, "getName", "()Ljava/lang/String;");
    Class_isArray = env->GetMethodID(Class, "isArray", "()Z");

    Double = globalize(env, env->FindClass("java/lang/Double"));
    Double_doubleValue = env->GetMethodID(Double, "doubleValue", "()D");
    Double_valueOf = env->GetStaticMethodID(Double, "valueOf", "(D)Ljava/lang/Double;");

    Enum = globalize(env, env->FindClass("java/lang/Enum"));
    Enum_ordinal = env->GetMethodID(Enum, "ordinal", "()I");

    Float = globalize(env, env->FindClass("java/lang/Float"));
    Float_floatValue = env->GetMethodID(Float, "floatValue", "()F");
    Float_valueOf = env->GetStaticMethodID(Float, "valueOf", "(F)Ljava/lang/Float;");

    HashMap = globalize(env, env->FindClass("java/util/HashMap"));
    HashMap_ctor = env->GetMethodID(HashMap, "<init>", "()V");
    HashMap_put = env->GetMethodID(HashMap, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

    HashSet = globalize(env, env->FindClass("java/util/HashSet"));
    HashSet_ctor = env->GetMethodID(HashSet, "<init>", "()V");
    HashSet_add = env->GetMethodID(HashSet, "add", "(Ljava/lang/Object;)Z");

    Integer = globalize(env, env->FindClass("java/lang/Integer"));
    Integer_intValue = env->GetMethodID(Integer, "intValue", "()I");
    Integer_valueOf = env->GetStaticMethodID(Integer, "valueOf", "(I)Ljava/lang/Integer;");

    Iterator = globalize(env, env->FindClass("java/util/Iterator"));
    Iterator_hasNext = env->GetMethodID(Iterator, "hasNext", "()Z");
    Iterator_next = env->GetMethodID(Iterator, "next", "()Ljava/lang/Object;");

    List = globalize(env, env->FindClass("java/util/List"));
    List_iterator = env->GetMethodID(List, "iterator", "()Ljava/util/Iterator;");
    List_size = env->GetMethodID(List, "size", "()I");

    Long = globalize(env, env->FindClass("java/lang/Long"));
    Long_longValue = env->GetMethodID(Long, "longValue", "()J");
    Long_valueOf = env->GetStaticMethodID(Long, "valueOf", "(J)Ljava/lang/Long;");

    Map = globalize(env, env->FindClass("java/util/Map"));

    Object = globalize(env, env->FindClass("java/lang/Object"));
    Object_getClass = env->GetMethodID(Object, "getClass", "()Ljava/lang/Class;");

    Ref = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/Ref"));
    Ref_ref = env->GetFieldID(Ref, "ref", "Ljava/lang/Object;");

    String = globalize(env, env->FindClass("java/lang/String"));

    GlibGSignalFlags = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GSignalFlags"));
    GlibGSignalFlags_RUN_FIRST = getEnumField(env, GlibGSignalFlags, "RUN_FIRST", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_RUN_LAST = getEnumField(env, GlibGSignalFlags, "RUN_LAST", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_RUN_CLEANUP = getEnumField(env, GlibGSignalFlags, "RUN_CLEANUP", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_NO_RECURSE = getEnumField(env, GlibGSignalFlags, "NO_RECURSE", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_DETAILED = getEnumField(env, GlibGSignalFlags, "DETAILED", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_ACTION = getEnumField(env, GlibGSignalFlags, "ACTION", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_NO_HOOKS = getEnumField(env, GlibGSignalFlags, "NO_HOOKS", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_MUST_COLLECT = getEnumField(env, GlibGSignalFlags, "MUST_COLLECT", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");
    GlibGSignalFlags_DEPRECATED = getEnumField(env, GlibGSignalFlags, "DEPRECATED", "Lme/r4zzz4k/girkt/gir/GSignalFlags;");

    GiFieldInfoFlags = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GiFieldInfoFlags"));
    GiFieldInfoFlags_IS_READABLE = getEnumField(env, GiFieldInfoFlags, "IS_READABLE", "Lme/r4zzz4k/girkt/gir/GiFieldInfoFlags;");
    GiFieldInfoFlags_IS_WRITABLE = getEnumField(env, GiFieldInfoFlags, "IS_WRITABLE", "Lme/r4zzz4k/girkt/gir/GiFieldInfoFlags;");

    GiFunctionInfoFlags = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GiFunctionInfoFlags"));
    GiFunctionInfoFlags_IS_METHOD = getEnumField(env, GiFunctionInfoFlags, "IS_METHOD", "Lme/r4zzz4k/girkt/gir/GiFunctionInfoFlags;");
    GiFunctionInfoFlags_IS_CONSTRUCTOR = getEnumField(env, GiFunctionInfoFlags, "IS_CONSTRUCTOR", "Lme/r4zzz4k/girkt/gir/GiFunctionInfoFlags;");
    GiFunctionInfoFlags_IS_GETTER = getEnumField(env, GiFunctionInfoFlags, "IS_GETTER", "Lme/r4zzz4k/girkt/gir/GiFunctionInfoFlags;");
    GiFunctionInfoFlags_IS_SETTER = getEnumField(env, GiFunctionInfoFlags, "IS_SETTER", "Lme/r4zzz4k/girkt/gir/GiFunctionInfoFlags;");
    GiFunctionInfoFlags_WRAPS_VFUNC = getEnumField(env, GiFunctionInfoFlags, "WRAPS_VFUNC", "Lme/r4zzz4k/girkt/gir/GiFunctionInfoFlags;");
    GiFunctionInfoFlags_THROWS = getEnumField(env, GiFunctionInfoFlags, "THROWS", "Lme/r4zzz4k/girkt/gir/GiFunctionInfoFlags;");

    GiVFuncInfoFlags = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GiVFuncInfoFlags"));
    GiVFuncInfoFlags_MUST_CHAIN_UP = getEnumField(env, GiVFuncInfoFlags, "MUST_CHAIN_UP", "Lme/r4zzz4k/girkt/gir/GiVFuncInfoFlags;");
    GiVFuncInfoFlags_MUST_OVERRIDE = getEnumField(env, GiVFuncInfoFlags, "MUST_OVERRIDE", "Lme/r4zzz4k/girkt/gir/GiVFuncInfoFlags;");
    GiVFuncInfoFlags_MUST_NOT_OVERRIDE = getEnumField(env, GiVFuncInfoFlags, "MUST_NOT_OVERRIDE", "Lme/r4zzz4k/girkt/gir/GiVFuncInfoFlags;");
    GiVFuncInfoFlags_THROWS = getEnumField(env, GiVFuncInfoFlags, "THROWS", "Lme/r4zzz4k/girkt/gir/GiVFuncInfoFlags;");

    GiInfoType = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GiInfoType"));
    GiInfoType_INVALID = getEnumField(env, GiInfoType, "INVALID", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_FUNCTION = getEnumField(env, GiInfoType, "FUNCTION", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_CALLBACK = getEnumField(env, GiInfoType, "CALLBACK", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_STRUCT = getEnumField(env, GiInfoType, "STRUCT", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_BOXED = getEnumField(env, GiInfoType, "BOXED", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_ENUM = getEnumField(env, GiInfoType, "ENUM", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_FLAGS = getEnumField(env, GiInfoType, "FLAGS", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_OBJECT = getEnumField(env, GiInfoType, "OBJECT", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_INTERFACE = getEnumField(env, GiInfoType, "INTERFACE", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_CONSTANT = getEnumField(env, GiInfoType, "CONSTANT", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_INVALID_0 = getEnumField(env, GiInfoType, "INVALID_0", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_UNION = getEnumField(env, GiInfoType, "UNION", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_VALUE = getEnumField(env, GiInfoType, "VALUE", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_SIGNAL = getEnumField(env, GiInfoType, "SIGNAL", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_VFUNC = getEnumField(env, GiInfoType, "VFUNC", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_PROPERTY = getEnumField(env, GiInfoType, "PROPERTY", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_FIELD = getEnumField(env, GiInfoType, "FIELD", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_ARG = getEnumField(env, GiInfoType, "ARG", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_TYPE = getEnumField(env, GiInfoType, "TYPE", "Lme/r4zzz4k/girkt/gir/GiInfoType;");
    GiInfoType_UNRESOLVED = getEnumField(env, GiInfoType, "UNRESOLVED", "Lme/r4zzz4k/girkt/gir/GiInfoType;");

    GiTransfer = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GiTransfer"));
    GiTransfer_NOTHING = getEnumField(env, GiTransfer, "NOTHING", "Lme/r4zzz4k/girkt/gir/GiTransfer;");
    GiTransfer_CONTAINER = getEnumField(env, GiTransfer, "CONTAINER", "Lme/r4zzz4k/girkt/gir/GiTransfer;");
    GiTransfer_EVERYTHING = getEnumField(env, GiTransfer, "EVERYTHING", "Lme/r4zzz4k/girkt/gir/GiTransfer;");

    GiTypeTag = globalize(env, env->FindClass("me/r4zzz4k/girkt/gir/GiTypeTag"));
    GiTypeTag_VOID = getEnumField(env, GiTypeTag, "VOID", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_BOOLEAN = getEnumField(env, GiTypeTag, "BOOLEAN", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_INT8 = getEnumField(env, GiTypeTag, "INT8", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_UINT8 = getEnumField(env, GiTypeTag, "UINT8", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_INT16 = getEnumField(env, GiTypeTag, "INT16", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_UINT16 = getEnumField(env, GiTypeTag, "UINT16", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_INT32 = getEnumField(env, GiTypeTag, "INT32", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_UINT32 = getEnumField(env, GiTypeTag, "UINT32", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_INT64 = getEnumField(env, GiTypeTag, "INT64", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_UINT64 = getEnumField(env, GiTypeTag, "UINT64", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_FLOAT = getEnumField(env, GiTypeTag, "FLOAT", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_DOUBLE = getEnumField(env, GiTypeTag, "DOUBLE", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_GTYPE = getEnumField(env, GiTypeTag, "GTYPE", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_UTF8 = getEnumField(env, GiTypeTag, "UTF8", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_FILENAME = getEnumField(env, GiTypeTag, "FILENAME", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_ARRAY = getEnumField(env, GiTypeTag, "ARRAY", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_INTERFACE = getEnumField(env, GiTypeTag, "INTERFACE", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_GLIST = getEnumField(env, GiTypeTag, "GLIST", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_GSLIST = getEnumField(env, GiTypeTag, "GSLIST", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_GHASH = getEnumField(env, GiTypeTag, "GHASH", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_ERROR = getEnumField(env, GiTypeTag, "ERROR", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");
    GiTypeTag_UNICHAR = getEnumField(env, GiTypeTag, "UNICHAR", "Lme/r4zzz4k/girkt/gir/GiTypeTag;");

    return true;
}
