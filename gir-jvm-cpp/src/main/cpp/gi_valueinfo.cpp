#include "gi.h"

GIValueInfo* getValueInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIValueInfo>(env, thiz, GiValueInfo_ptr);
}

JNIEXPORT jlong JNICALL GiValueInfo_getValue(JNIEnv* env, jobject thiz) {
    auto info = getValueInfo(env, thiz);
    return g_value_info_get_value(info);
}
