#include "gi.h"

#define registerNativeClass(env, clazz) \
        (registerNativeMethods(env, clazz ## _className, clazz ## _methods, sizeof(clazz ## _methods) / sizeof(JNINativeMethod)))

bool fillCache(JNIEnv* env);

static bool registerNativeMethods(JNIEnv* env, const char* className, JNINativeMethod* methods, int numMethods) {
    jclass clazz;
    clazz = env->FindClass(className);
    if (clazz == NULL) {
        fprintf(stderr, "Native registration unable to find class '%s'\n", className);
        return false;
    }
    if (env->RegisterNatives(clazz, methods, numMethods) < 0) {
        fprintf(stderr, "RegisterNatives failed for '%s'\n", className);
        return false;
    }
    return true;
}

static bool registerNativeClasses(JNIEnv* env);

#include "cache.inc"
#include "methods.inc"
#include "onload.h"

bool init(JNIEnv* env) {
    if(!fillCache(env)) {
        fprintf(stderr, "ERROR: JNI caching failed\n");
        return false;
    }

    if(!registerNativeClasses(env)) {
        fprintf(stderr, "ERROR: Native methods registration failed\n");
        return false;
    }
    return true;
}

jint JNI_OnLoad(JavaVM* vm, void* /*reserved*/) {
    setbuf(stdout, NULL);
    JNIEnv* env = nullptr;

    fprintf(stdout, "JNI_OnLoad\n");
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_4) != JNI_OK) {
        fprintf(stderr, "ERROR: GetEnv failed\n");
        return -1;
    }

    if(!init(env)) {
        return -1;
    }

    fprintf(stdout, "JNI_OnLoad finished\n");
    return JNI_VERSION_1_4;
}
