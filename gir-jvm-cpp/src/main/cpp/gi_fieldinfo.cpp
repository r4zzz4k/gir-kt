#include "gi.h"

GIFieldInfo* getFieldInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIFieldInfo>(env, thiz, GiFieldInfo_ptr);
}

JNIEXPORT jobject JNICALL GiFieldInfo_getFlags(JNIEnv* env, jobject thiz) {
    auto info = getFieldInfo(env, thiz);
    auto result = g_field_info_get_flags(info);
    return convertGiFieldInfoFlags(env, result);
}
