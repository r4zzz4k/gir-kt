#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "gi.h"

GIRepository* getRepository(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIRepository>(env, thiz, GiRepository_ptr);
}

JNIEXPORT void JNICALL GiRepository_require(JNIEnv* env, jobject thiz, jstring namespace_, jstring version, jboolean lazy) {
    auto repo = getRepository(env, thiz);
    auto namespacePtr = env->GetStringUTFChars(namespace_, nullptr);
    auto versionPtr = version ? env->GetStringUTFChars(version, nullptr) : nullptr;

    fputs("Typelib search paths:\n", stderr);
    g_slist_foreach(g_irepository_get_search_path(), [](gpointer data, gpointer userData) {
        fprintf(stderr, "\t[%s]\n", (char const*) data);
    }, nullptr);

    GError* error = nullptr;
    auto result = g_irepository_require(
            repo,
            namespacePtr,
            versionPtr,
            lazy ? G_IREPOSITORY_LOAD_FLAG_LAZY : (GIRepositoryLoadFlags) 0,
            &error);

    if(!result) {
        fprintf(stderr, "Loading %s-%s failed: %s\n", namespacePtr, versionPtr, error->message);
    }

    env->ReleaseStringUTFChars(namespace_, namespacePtr);
    if(version) {
        env->ReleaseStringUTFChars(version, versionPtr);
    }
}

JNIEXPORT jobject JNICALL GiRepository_getSharedLibraries(JNIEnv* env, jobject thiz, jstring namespace_) {
    auto repo = getRepository(env, thiz);
    auto namespacePtr = env->GetStringUTFChars(namespace_, nullptr);

    auto resultPtr = g_irepository_get_shared_library(repo, namespacePtr);

    env->ReleaseStringUTFChars(namespace_, namespacePtr);

    return populateList(env, [&](auto add) {
        if(resultPtr) {
            auto len = strlen(resultPtr);
            auto copy = reinterpret_cast<char*>(malloc((len + 1) * sizeof(char)));
            strncpy(copy, resultPtr, len);
            copy[len] = '\0';

            char* ch = strtok(copy, ",");
            while (ch) {
                add(env->NewStringUTF(ch));
                ch = strtok(nullptr, ",");
            }

            free(copy);
        }
    });
}

JNIEXPORT jobject JNICALL GiRepository_getInfos(JNIEnv* env, jobject thiz, jstring namespace_) {
    auto repo = getRepository(env, thiz);
    auto namespacePtr = env->GetStringUTFChars(namespace_, nullptr);

    auto result = populateList(env, [&](auto add) {
        auto count = g_irepository_get_n_infos(repo, namespacePtr);
        for(auto i = 0; i < count; ++i) {
            auto info = g_irepository_get_info(repo, namespacePtr, i);
            auto type = g_base_info_get_type(info);

            //printf("\n%s %d", g_base_info_get_name(info), type);

            jclass clazz;
            jmethodID ctor;
            switch(type) {
                case GI_INFO_TYPE_FUNCTION: clazz = GiFunctionInfo; ctor = GiFunctionInfo_ctor; break;
                case GI_INFO_TYPE_CALLBACK: clazz = GiCallbackInfo; ctor = GiCallbackInfo_ctor; break;
                case GI_INFO_TYPE_STRUCT: clazz = GiStructInfo; ctor = GiStructInfo_ctor; break;
                //case GI_INFO_TYPE_BOXED: STRUCT OR UNION clazz = GiStructInfo; ctor = GiStructInfo_ctor; break;
                case GI_INFO_TYPE_ENUM: clazz = GiEnumInfo; ctor = GiEnumInfo_ctor; break;
                //case GI_INFO_TYPE_FLAGS: clazz = GiEnumInfo; ctor = GiEnumInfo_ctor; break;
                case GI_INFO_TYPE_OBJECT: clazz = GiObjectInfo; ctor = GiObjectInfo_ctor; break;
                case GI_INFO_TYPE_INTERFACE: clazz = GiInterfaceInfo; ctor = GiInterfaceInfo_ctor; break;
                case GI_INFO_TYPE_CONSTANT: clazz = GiConstantInfo; ctor = GiConstantInfo_ctor; break;
                case GI_INFO_TYPE_UNION: clazz = GiUnionInfo; ctor = GiUnionInfo_ctor; break;
                case GI_INFO_TYPE_VALUE: clazz = GiValueInfo; ctor = GiValueInfo_ctor; break;
                case GI_INFO_TYPE_SIGNAL: clazz = GiSignalInfo; ctor = GiSignalInfo_ctor; break;
                case GI_INFO_TYPE_VFUNC: clazz = GiVFuncInfo; ctor = GiVFuncInfo_ctor; break;
                case GI_INFO_TYPE_PROPERTY: clazz = GiPropertyInfo; ctor = GiPropertyInfo_ctor; break;
                case GI_INFO_TYPE_FIELD: clazz = GiFieldInfo; ctor = GiFieldInfo_ctor; break;
                case GI_INFO_TYPE_ARG: clazz = GiArgInfo; ctor = GiArgInfo_ctor; break;
                case GI_INFO_TYPE_TYPE: clazz = GiTypeInfo; ctor = GiTypeInfo_ctor; break;
                default: clazz = GiBaseInfo; ctor = GiBaseInfo_ctor; break;
            }
            //printf(" constructing");
            auto object = constructObjectWithNativePtr(env, clazz, ctor, info);
            //printf(" adding");
            add(object);
        }
    });

    env->ReleaseStringUTFChars(namespace_, namespacePtr);

    return result;
}
