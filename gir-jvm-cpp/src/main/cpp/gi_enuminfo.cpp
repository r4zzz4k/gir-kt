#include "gi.h"

GIEnumInfo* getEnumInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIEnumInfo>(env, thiz, GiEnumInfo_ptr);
}

JNIEXPORT jobject JNICALL GiEnumInfo_getValues(JNIEnv* env, jobject thiz) {
    auto info = getEnumInfo(env, thiz);
    return populateList(env, [&](auto add) {
        auto count = g_enum_info_get_n_values(info);
        for(auto i = 0; i < count; ++i) {
            auto value = g_enum_info_get_value(info, i);
            auto object = constructObjectWithNativePtr(env, GiValueInfo, GiValueInfo_ctor, value);
            add(object);
        }
    });
}

JNIEXPORT jobject JNICALL GiEnumInfo_getMethods(JNIEnv* env, jobject thiz) {
    auto info = getEnumInfo(env, thiz);
    return populateList(env, [&](auto add) {
        auto count = g_enum_info_get_n_methods(info);
        for(auto i = 0; i < count; ++i) {
            auto method = g_enum_info_get_method(info, i);
            auto object = constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, method);
            add(object);
        }
    });
}

JNIEXPORT jobject JNICALL GiEnumInfo_getStorageType(JNIEnv* env, jobject thiz) {
    auto info = getEnumInfo(env, thiz);
    auto storageType = g_enum_info_get_storage_type(info);
    return convertGiTypeTag(env, storageType);
}

JNIEXPORT jstring JNICALL GiEnumInfo_getErrorDomain(JNIEnv* env, jobject thiz) {
    auto info = getEnumInfo(env, thiz);
    auto resultPtr = g_enum_info_get_error_domain(info);
    return env->NewStringUTF(resultPtr);
}
