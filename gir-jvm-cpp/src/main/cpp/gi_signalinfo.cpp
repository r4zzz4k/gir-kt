#include "gi.h"

GISignalInfo* getSignalInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GISignalInfo>(env, thiz, GiSignalInfo_ptr);
}

JNIEXPORT jobject JNICALL GiSignalInfo_getFlags(JNIEnv* env, jobject thiz) {
    auto info = getSignalInfo(env, thiz);
    auto result = g_signal_info_get_flags(info);
    return convertGSignalFlags(env, result);
}

JNIEXPORT jobject JNICALL GiSignalInfo_getClassClosure(JNIEnv* env, jobject thiz) {
    auto info = getSignalInfo(env, thiz);
    auto result = g_signal_info_get_class_closure(info);
    return constructObjectWithNativePtr(env, GiVFuncInfo, GiVFuncInfo_ctor, result);
}

JNIEXPORT jboolean JNICALL GiSignalInfo_getTrueStopsEmit(JNIEnv* env, jobject thiz) {
    auto info = getSignalInfo(env, thiz);
    return g_signal_info_true_stops_emit(info);
}
