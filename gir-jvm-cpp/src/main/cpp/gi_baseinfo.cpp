#include "gi.h"

GIBaseInfo* getBaseInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIBaseInfo>(env, thiz, GiBaseInfo_ptr);
}

JNIEXPORT jobject JNICALL GiBaseInfo_getInfoType(JNIEnv* env, jobject thiz) {
    auto info = getBaseInfo(env, thiz);
    auto result = g_base_info_get_type(info);
    return convertGiInfoType(env, result);
}

JNIEXPORT jstring JNICALL GiBaseInfo_getNamespace(JNIEnv* env, jobject thiz) {
    auto info = getBaseInfo(env, thiz);
    auto result = g_base_info_get_namespace(info);
    return env->NewStringUTF(result);
}

JNIEXPORT jstring JNICALL GiBaseInfo_getName(JNIEnv* env, jobject thiz) {
    auto info = getBaseInfo(env, thiz);
    auto result = g_base_info_get_name(info);
    return env->NewStringUTF(result);
}

JNIEXPORT jobject JNICALL GiBaseInfo_getAttributes(JNIEnv* env, jobject thiz) {
    auto info = getBaseInfo(env, thiz);
    return populateMap(env, [&](auto put) {
        GIAttributeIter iter = { 0, };
        char* name;
        char* value;
        while (g_base_info_iterate_attributes(info, &iter, &name, &value)) {
            put(env->NewStringUTF(name), env->NewStringUTF(value));
        }
    });
}
