#include "gi.h"

GIStructInfo* getStructInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIStructInfo>(env, thiz, GiStructInfo_ptr);
}

JNIEXPORT jint JNICALL GiStructInfo_getAlignment(JNIEnv* env, jobject thiz) {
    auto info = getStructInfo(env, thiz);
    return g_struct_info_get_alignment(info);
}

JNIEXPORT jint JNICALL GiStructInfo_getSize(JNIEnv* env, jobject thiz) {
    auto info = getStructInfo(env, thiz);
    return g_struct_info_get_size(info);
}

JNIEXPORT jboolean JNICALL GiStructInfo_isGtypeStruct(JNIEnv* env, jobject thiz) {
    auto info = getStructInfo(env, thiz);
    return g_struct_info_is_gtype_struct(info);
}

JNIEXPORT jboolean JNICALL GiStructInfo_getForeign(JNIEnv* env, jobject thiz) {
    auto info = getStructInfo(env, thiz);
    return g_struct_info_is_gtype_struct(info);
}

JNIEXPORT jobject JNICALL GiStructInfo_getFields(JNIEnv* env, jobject thiz) {
    auto info = getStructInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_struct_info_get_n_fields(info); ++i) {
            auto result = g_struct_info_get_field(info, i);
            add(constructObjectWithNativePtr(env, GiFieldInfo, GiFieldInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiStructInfo_getMethods(JNIEnv* env, jobject thiz) {
    auto info = getStructInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_struct_info_get_n_methods(info); ++i) {
            auto result = g_struct_info_get_method(info, i);
            add(constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiStructInfo_findMethod(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getStructInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_struct_info_find_method(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result);
}
