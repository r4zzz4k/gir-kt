#include "gi.h"
#include <cstring>
#include <cstdio>

// https://github.com/creationix/node-gir/blob/da0b211c1d0b2243edb9cb86b293706e0b94ee14/src/arguments.cc

static void printTypeError(JNIEnv* env, GIArgInfo* argInfo, GITypeTag expected, jobject actual) {
    fprintf(stderr, "Invalid type passed for %s: expected %s, got ", g_base_info_get_name(argInfo), g_type_tag_to_string(expected));
    printClassName(stderr, env, actual);
    fputs("\n", stderr);
}
static void printUnimplementedError(JNIEnv* env, GIArgInfo* argInfo, GITypeTag expected, jobject actual) {
    fprintf(stderr, "Unimplemented type for %s: %s, got ", g_base_info_get_name(argInfo), g_type_tag_to_string(expected));
    printClassName(stderr, env, actual);
    fputs("\n", stderr);
}

bool convertGiValue(JNIEnv* env, jobject input, GValue* output) {
    auto inputClass = env->GetObjectClass(input);

    if(env->IsInstanceOf(input, Boolean)) {
        auto value = env->CallBooleanMethod(input, Boolean_booleanValue);
        g_value_init(output, G_TYPE_BOOLEAN);
        g_value_set_boolean(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, Character)) {
        auto value = env->CallCharMethod(input, Character_charValue);
        g_value_init(output, G_TYPE_CHAR);
        g_value_set_char(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, Integer)) {
        auto value = env->CallIntMethod(input, Integer_intValue);
        g_value_init(output, G_TYPE_INT);
        g_value_set_int(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, Long)) {
        auto value = env->CallLongMethod(input, Long_longValue);
        g_value_init(output, G_TYPE_LONG);
        g_value_set_long(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, Float)) {
        auto value = env->CallFloatMethod(input, Float_floatValue);
        g_value_init(output, G_TYPE_FLOAT);
        g_value_set_float(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, Double)) {
        auto value = env->CallDoubleMethod(input, Double_doubleValue);
        g_value_init(output, G_TYPE_DOUBLE);
        g_value_set_double(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, Enum)) {
        auto value = env->CallIntMethod(input, Enum_ordinal);
        g_value_init(output, G_TYPE_ENUM);
        g_value_set_enum(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, String)) {
        auto value = env->GetStringUTFChars((jstring) input, nullptr);
        g_value_init(output, G_TYPE_STRING);
        g_value_set_string(output, value);
        env->ReleaseStringUTFChars((jstring) input, value);
        return true;
    }
    if(env->IsInstanceOf(input, GPointer)) {
        auto value = (gpointer) env->GetLongField(input, GPointer_ptr);
        g_value_init(output, G_TYPE_POINTER);
        g_value_set_pointer(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, GObjectRef)) {
        auto value = (gpointer) env->GetLongField(input, GObjectRef_ptr);
        g_value_init(output, G_TYPE_OBJECT);
        g_value_set_pointer(output, value);
        return true;
    }
    if(env->IsInstanceOf(input, GTypeRef)) {
        auto value = (GType) env->GetLongField(input, GTypeRef_ptr);
        g_value_init(output, G_TYPE_GTYPE);
        g_value_set_gtype(output, value);
        return true;
    }

    return false;
}
