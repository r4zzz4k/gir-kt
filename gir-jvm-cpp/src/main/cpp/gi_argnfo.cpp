#include "gi.h"

GIArgInfo* getArgInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIArgInfo>(env, thiz, GiArgInfo_ptr);
}

JNIEXPORT jint JNICALL GiArgInfo_getClosure(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_get_closure(info);
}

JNIEXPORT jint JNICALL GiArgInfo_getDestroy(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_get_destroy(info);
}

JNIEXPORT jobject JNICALL GiArgInfo_getDirection(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    auto result = g_arg_info_get_direction(info);
    return convertGiDirection(env, result);
}

JNIEXPORT jobject JNICALL GiArgInfo_getOwnershipTransfer(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    auto result = g_arg_info_get_ownership_transfer(info);
    return convertGiTransfer(env, result);
}

JNIEXPORT jobject JNICALL GiArgInfo_getScope(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    auto result = g_arg_info_get_scope(info);
    return convertGiScopeType(env, result);
}

JNIEXPORT jobject JNICALL GiArgInfo_getType(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    auto result = g_arg_info_get_type(info);
    return constructObjectWithNativePtr(env, GiTypeInfo, GiTypeInfo_ctor, result);
}

JNIEXPORT jboolean JNICALL GiArgInfo_getNullable(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_may_be_null(info);
}

JNIEXPORT jboolean JNICALL GiArgInfo_getCallerAllocates(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_is_caller_allocates(info);
}

JNIEXPORT jboolean JNICALL GiArgInfo_getOptional(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_is_optional(info);
}

JNIEXPORT jboolean JNICALL GiArgInfo_isReturnValue(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_is_return_value(info);
}

JNIEXPORT jboolean JNICALL GiArgInfo_getSkip(JNIEnv* env, jobject thiz) {
    auto info = getArgInfo(env, thiz);
    return g_arg_info_is_skip(info);
}
