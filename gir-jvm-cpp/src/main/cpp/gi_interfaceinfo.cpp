#include "gi.h"

GIInterfaceInfo* getInterfaceInfo(JNIEnv* env, jobject thiz) {
    return getNativePtr<GIInterfaceInfo>(env, thiz, GiInterfaceInfo_ptr);
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getPrerequisites(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_interface_info_get_n_prerequisites(info); ++i) {
            auto result = g_interface_info_get_prerequisite(info, i);
            add(constructObjectWithNativePtr(env, GiBaseInfo, GiBaseInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getProperties(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_interface_info_get_n_properties(info); ++i) {
            auto result = g_interface_info_get_property(info, i);
            add(constructObjectWithNativePtr(env, GiPropertyInfo, GiPropertyInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getMethods(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_interface_info_get_n_methods(info); ++i) {
            auto result = g_interface_info_get_method(info, i);
            add(constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_findMethod(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getInterfaceInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_interface_info_find_method(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiFunctionInfo, GiFunctionInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getSignals(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_interface_info_get_n_signals(info); ++i) {
            auto result = g_interface_info_get_signal(info, i);
            add(constructObjectWithNativePtr(env, GiSignalInfo, GiSignalInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_findSignal(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getInterfaceInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_interface_info_find_signal(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiSignalInfo, GiSignalInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getVfuncs(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_interface_info_get_n_vfuncs(info); ++i) {
            auto result = g_interface_info_get_vfunc(info, i);
            add(constructObjectWithNativePtr(env, GiVFuncInfo, GiVFuncInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_findVfunc(JNIEnv* env, jobject thiz, jstring name) {
    auto info = getInterfaceInfo(env, thiz);
    auto namePtr = env->GetStringUTFChars(name, nullptr);
    auto result = g_interface_info_find_vfunc(info, namePtr);
    env->ReleaseStringUTFChars(name, namePtr);
    return constructObjectWithNativePtr(env, GiVFuncInfo, GiVFuncInfo_ctor, result);
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getConstants(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    return populateList(env, [&](auto add) {
        for(auto i = 0; i < g_interface_info_get_n_constants(info); ++i) {
            auto result = g_interface_info_get_constant(info, i);
            add(constructObjectWithNativePtr(env, GiConstantInfo, GiConstantInfo_ctor, result));
        }
    });
}

JNIEXPORT jobject JNICALL GiInterfaceInfo_getIfaceStruct(JNIEnv* env, jobject thiz) {
    auto info = getInterfaceInfo(env, thiz);
    auto result = g_interface_info_get_iface_struct(info);
    return constructObjectWithNativePtr(env, GiStructInfo, GiStructInfo_ctor, result);
}
