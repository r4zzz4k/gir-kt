#ifndef __METHODS_REGISTERED_H__
#define __METHODS_REGISTERED_H__

#include <jni.h>
#include <girepository.h>

JNIEXPORT jstring JNICALL GiRegisteredTypeInfo_getTypeName(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiRegisteredTypeInfo_getTypeInit(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiRegisteredTypeInfo_getGType(JNIEnv* env, jobject thiz);

JNIEXPORT jobject JNICALL GiEnumInfo_getValues(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiEnumInfo_getMethods(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiEnumInfo_getStorageType(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiEnumInfo_getErrorDomain(JNIEnv* env, jobject thiz);

JNIEXPORT jobject JNICALL GiInterfaceInfo_getPrerequisites(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiInterfaceInfo_getProperties(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiInterfaceInfo_getMethods(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiInterfaceInfo_findMethod(JNIEnv* env, jobject thiz, jstring name);
JNIEXPORT jobject JNICALL GiInterfaceInfo_getSignals(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiInterfaceInfo_findSignal(JNIEnv* env, jobject thiz, jstring name);
JNIEXPORT jobject JNICALL GiInterfaceInfo_getVfuncs(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiInterfaceInfo_findVfunc(JNIEnv* env, jobject thiz, jstring name);
JNIEXPORT jobject JNICALL GiInterfaceInfo_getConstants(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiInterfaceInfo_getIfaceStruct(JNIEnv* env, jobject thiz);

JNIEXPORT jboolean JNICALL GiObjectInfo_getAbstract(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiObjectInfo_getFundamental(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_getParent(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiObjectInfo_getTypeName(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiObjectInfo_getTypeInit(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_getConstants(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_getFields(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_getInterfaces(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_getMethods(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_findMethod(JNIEnv* env, jobject thiz, jstring name);
JNIEXPORT jobject JNICALL GiObjectInfo_getProperties(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_getSignals(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_findSignal(JNIEnv* env, jobject thiz, jstring name);
JNIEXPORT jobject JNICALL GiObjectInfo_getVfuncs(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiObjectInfo_findVfunc(JNIEnv* env, jobject thiz, jstring name);
JNIEXPORT jobject JNICALL GiObjectInfo_getClassStruct(JNIEnv* env, jobject thiz);

JNIEXPORT jint JNICALL GiStructInfo_getAlignment(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiStructInfo_getSize(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiStructInfo_isGtypeStruct(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiStructInfo_getForeign(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiStructInfo_getFields(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiStructInfo_getMethods(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiStructInfo_findMethod(JNIEnv* env, jobject thiz, jstring name);

#endif /*__METHODS_REGISTERED_H__*/
