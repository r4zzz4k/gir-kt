#ifndef __METHODS_H__
#define __METHODS_H__

#include <jni.h>
#include <girepository.h>

JNIEXPORT jobject JNICALL GiRepositoryKt_giRepository(JNIEnv* env, jclass clazz);

JNIEXPORT jstring JNICALL EnumsKt_giInfoTypeToString(JNIEnv* env, jclass clazz, jobject thiz);
JNIEXPORT jstring JNICALL EnumsKt_giTypeTagToString(JNIEnv* env, jclass clazz, jobject thiz);

JNIEXPORT void JNICALL GiRepository_require(JNIEnv* env, jobject thiz, jstring namespace_, jstring version, jboolean lazy);
JNIEXPORT jobject JNICALL GiRepository_getSharedLibraries(JNIEnv* env, jobject thiz, jstring namespace_);
JNIEXPORT jobject JNICALL GiRepository_getInfos(JNIEnv* env, jobject thiz, jstring namespace_);

JNIEXPORT jint JNICALL GiArgInfo_getClosure(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgInfo_getDestroy(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiArgInfo_getDirection(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiArgInfo_getOwnershipTransfer(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiArgInfo_getScope(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiArgInfo_getType(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiArgInfo_getNullable(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiArgInfo_getCallerAllocates(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiArgInfo_getOptional(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiArgInfo_isReturnValue(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiArgInfo_getSkip(JNIEnv* env, jobject thiz);

JNIEXPORT jobject JNICALL GiBaseInfo_getInfoType(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiBaseInfo_getNamespace(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiBaseInfo_getName(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiBaseInfo_getAttributes(JNIEnv* env, jobject thiz);

JNIEXPORT jobject JNICALL GiConstantInfo_getType(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiConstantInfo_getValue(JNIEnv* env, jobject thiz);
JNIEXPORT void JNICALL GiConstantInfo_freeValue(JNIEnv* env, jobject thiz, jobject value);

JNIEXPORT jobject JNICALL GiFieldInfo_getFlags(JNIEnv* env, jobject thiz);

JNIEXPORT jboolean JNICALL GiTypeInfo_isPointer(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiTypeInfo_getTag(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiTypeInfo_paramType(JNIEnv* env, jobject thiz, jint num);
JNIEXPORT jobject JNICALL GiTypeInfo_getIface(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiTypeInfo_getArrayLength(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiTypeInfo_getArrayFixedSize(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiTypeInfo_isZeroTerminated(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiTypeInfo_getArrayType(JNIEnv* env, jobject thiz);

JNIEXPORT jlong JNICALL GiValueInfo_getValue(JNIEnv* env, jobject thiz);

#include "methods_callable.h"
#include "methods_registered.h"
#include "collections.h"

JNIEXPORT jboolean JNICALL GiArgument_getVBoolean(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVInt8(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVUint8(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVInt16(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVUint16(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVInt32(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVUint32(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVInt64(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVUint64(JNIEnv* env, jobject thiz);
JNIEXPORT jfloat JNICALL GiArgument_getVFloat(JNIEnv* env, jobject thiz);
JNIEXPORT jdouble JNICALL GiArgument_getVDouble(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVShort(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiArgument_getVUshort(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVLong(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVUlong(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVSsize(JNIEnv* env, jobject thiz);
JNIEXPORT jlong JNICALL GiArgument_getVSize(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiArgument_getVString(JNIEnv* env, jobject thiz);
//JNIEXPORT jobject JNICALL GiArgument_getVPointer(JNIEnv* env, jobject thiz);

bool convertGiArgument(JNIEnv* env, jobject input, GIArgument* output, GITypeInfo* typeInfo, bool mayBeNull, char const* argName);
jobject convertGiArgument(JNIEnv* env, GIArgument* input, GITypeInfo* typeInfo, char const* argName);
bool convertGiValue(JNIEnv* env, jobject input, GValue* output);

jobject convertGSignalFlags(JNIEnv* env, GSignalFlags input);
jobject convertGiArrayType(JNIEnv* env, GIArrayType input);
jobject convertGiDirection(JNIEnv* env, GIDirection input);
jobject convertGiFieldInfoFlags(JNIEnv* env, GIFieldInfoFlags input);
jobject convertGiScopeType(JNIEnv* env, GIScopeType input);
jobject convertGiTransfer(JNIEnv* env, GITransfer input);
jobject convertGiFunctionInfoFlags(JNIEnv* env, GIFunctionInfoFlags input);
jobject convertGiVFuncInfoFlags(JNIEnv* env, GIVFuncInfoFlags input);
jobject convertGiInfoType(JNIEnv* env, GIInfoType input);
GIInfoType convertGiInfoType(JNIEnv* env, jobject input);
jobject convertGiTypeTag(JNIEnv* env, GITypeTag input);
GITypeTag convertGiTypeTag(JNIEnv* env, jobject input);

#endif /*__METHODS_H__*/
