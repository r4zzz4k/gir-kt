#ifndef __METHODS_CALLABLE_H__
#define __METHODS_CALLABLE_H__

#include <jni.h>
#include <girepository.h>

JNIEXPORT jboolean JNICALL GiCallableInfo_getThrows(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiCallableInfo_getArgs(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiCallableInfo_getCallerOwns(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiCallableInfo_getReturnAttributes(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiCallableInfo_getReturnType(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiCallableInfo_getIsMethod(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiCallableInfo_getReturnNullable(JNIEnv* env, jobject thiz);

JNIEXPORT jobject JNICALL GiFunctionInfo_getFlags(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiFunctionInfo_getProperty(JNIEnv* env, jobject thiz);
JNIEXPORT jstring JNICALL GiFunctionInfo_getSymbol(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiFunctionInfo_getVfunc(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiFunctionInfo_invoke(JNIEnv* env, jobject thiz, jobjectArray args /* TODO */);

JNIEXPORT jobject JNICALL GiSignalInfo_getFlags(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiSignalInfo_getClassClosure(JNIEnv* env, jobject thiz);
JNIEXPORT jboolean JNICALL GiSignalInfo_getTrueStopsEmit(JNIEnv* env, jobject thiz);

JNIEXPORT jobject JNICALL GiVFuncInfo_getFlags(JNIEnv* env, jobject thiz);
JNIEXPORT jint JNICALL GiVFuncInfo_getOffset(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiVFuncInfo_getSignal(JNIEnv* env, jobject thiz);
JNIEXPORT jobject JNICALL GiVFuncInfo_getInvoker(JNIEnv* env, jobject thiz);

#endif /*__METHODS_CALLABLE_H__*/
