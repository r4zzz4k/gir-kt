#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include <jni.h>

template<typename T>
static T globalize(JNIEnv* env, T reference) {
    T globalRef = reinterpret_cast<T>(env->NewGlobalRef(reference));
    env->DeleteLocalRef(reference);
    return globalRef;
}

inline static void printClassName(FILE* file, JNIEnv* env, jobject object) {
    if(!object) {
        fputs("[null]", file);
    } else {
        jobject clazz = env->CallObjectMethod(object, Object_getClass);
        jstring name = (jstring) env->CallObjectMethod(clazz, Class_getName);

        auto namePtr = env->GetStringUTFChars(name, nullptr);
        fputs(namePtr, file);
        env->ReleaseStringUTFChars(name, namePtr);
    }
}

template<typename T>
T* getNativePtr(JNIEnv* env, jobject object, jfieldID ptrField) {
    auto ptr = env->GetLongField(object, ptrField);
    return reinterpret_cast<T*>(ptr);
}

inline jobject constructObjectWithNativeLong(JNIEnv* env, jclass clazz, jmethodID ctor, long ptr) {
    return ptr ? env->NewObject(clazz, ctor, ptr) : nullptr;
}
template<typename T>
jobject constructObjectWithNativePtr(JNIEnv* env, jclass clazz, jmethodID ctor, T* ptr) {
    return constructObjectWithNativeLong(env, clazz, ctor, reinterpret_cast<long>(ptr));
}

template<typename Cb>
jobject populateList(JNIEnv* env, Cb callback) {
    auto result = env->NewObject(ArrayList, ArrayList_ctor);
    callback([&](jobject object) { env->CallBooleanMethod(result, ArrayList_add, object); });
    return result;
}

template<typename Cb>
jobject populateMap(JNIEnv* env, Cb callback) {
    auto result = env->NewObject(HashMap, HashMap_ctor);
    callback([&](jobject key, jobject value) { env->CallObjectMethod(result, HashMap_put, key, value); });
    return result;
}

template<typename Cb>
jobject populateSet(JNIEnv* env, Cb callback) {
    auto result = env->NewObject(HashSet, HashSet_ctor);
    callback([&](jobject value) { env->CallObjectMethod(result, HashSet_add, value); });
    return result;
}

template<typename Cb>
void listForEach(JNIEnv* env, jobject input, Cb callback) {
    auto iterator = env->CallObjectMethod(input, List_iterator);
    while(env->CallBooleanMethod(iterator, Iterator_hasNext)) {
        auto element = env->CallObjectMethod(iterator, Iterator_next);
        callback(element);
    }
}

#endif /*__UTILITIES_H__*/
