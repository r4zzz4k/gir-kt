#define DEF_EXTERN_NATIVE_PTR_CLASS(shortName) \
        DEF_EXTERN jclass shortName; \
        DEF_EXTERN jmethodID shortName ## _ctor; \
        DEF_EXTERN jfieldID shortName ## _ptr;

DEF_EXTERN_NATIVE_PTR_CLASS(GObjectRef);
DEF_EXTERN_NATIVE_PTR_CLASS(GPointer);
DEF_EXTERN_NATIVE_PTR_CLASS(GTypeRef);
DEF_EXTERN_NATIVE_PTR_CLASS(GiArgInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiArgument);
DEF_EXTERN_NATIVE_PTR_CLASS(GiBaseInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiCallableInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiCallbackInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiConstantInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiEnumInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiFieldInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiFunctionInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiInterfaceInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiObjectInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiPropertyInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiRegisteredTypeInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiRepository);
DEF_EXTERN_NATIVE_PTR_CLASS(GiSignalInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiStructInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiTypeInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiUnionInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiVFuncInfo);
DEF_EXTERN_NATIVE_PTR_CLASS(GiValueInfo);

DEF_EXTERN jclass Array;
DEF_EXTERN jmethodID Array_get;
DEF_EXTERN jmethodID Array_getLength;

DEF_EXTERN jclass ArrayList;
DEF_EXTERN jmethodID ArrayList_ctor;
DEF_EXTERN jmethodID ArrayList_add;

DEF_EXTERN jclass Boolean;
DEF_EXTERN jmethodID Boolean_booleanValue;
DEF_EXTERN jmethodID Boolean_valueOf;

DEF_EXTERN jclass Character;
DEF_EXTERN jmethodID Character_charValue;
DEF_EXTERN jmethodID Character_valueOf;

DEF_EXTERN jclass Class;
DEF_EXTERN jmethodID Class_getName;
DEF_EXTERN jmethodID Class_isArray;

DEF_EXTERN jclass Double;
DEF_EXTERN jmethodID Double_doubleValue;
DEF_EXTERN jmethodID Double_valueOf;

DEF_EXTERN jclass Enum;
DEF_EXTERN jmethodID Enum_ordinal;

DEF_EXTERN jclass Float;
DEF_EXTERN jmethodID Float_floatValue;
DEF_EXTERN jmethodID Float_valueOf;

DEF_EXTERN jclass HashMap;
DEF_EXTERN jmethodID HashMap_ctor;
DEF_EXTERN jmethodID HashMap_put;

DEF_EXTERN jclass HashSet;
DEF_EXTERN jmethodID HashSet_ctor;
DEF_EXTERN jmethodID HashSet_add;

DEF_EXTERN jclass Integer;
DEF_EXTERN jmethodID Integer_intValue;
DEF_EXTERN jmethodID Integer_valueOf;

DEF_EXTERN jclass Iterator;
DEF_EXTERN jmethodID Iterator_hasNext;
DEF_EXTERN jmethodID Iterator_next;

DEF_EXTERN jclass List;
DEF_EXTERN jmethodID List_iterator;
DEF_EXTERN jmethodID List_size;

DEF_EXTERN jclass Long;
DEF_EXTERN jmethodID Long_longValue;
DEF_EXTERN jmethodID Long_valueOf;

DEF_EXTERN jclass Map;

DEF_EXTERN jclass Object;
DEF_EXTERN jmethodID Object_getClass;

DEF_EXTERN jclass Ref;
DEF_EXTERN jfieldID Ref_ref;

DEF_EXTERN jclass String;

DEF_EXTERN jclass GlibGSignalFlags;
DEF_EXTERN jobject GlibGSignalFlags_RUN_FIRST;
DEF_EXTERN jobject GlibGSignalFlags_RUN_LAST;
DEF_EXTERN jobject GlibGSignalFlags_RUN_CLEANUP;
DEF_EXTERN jobject GlibGSignalFlags_NO_RECURSE;
DEF_EXTERN jobject GlibGSignalFlags_DETAILED;
DEF_EXTERN jobject GlibGSignalFlags_ACTION;
DEF_EXTERN jobject GlibGSignalFlags_NO_HOOKS;
DEF_EXTERN jobject GlibGSignalFlags_MUST_COLLECT;
DEF_EXTERN jobject GlibGSignalFlags_DEPRECATED;

DEF_EXTERN jclass GiArrayType;
DEF_EXTERN jobject GiArrayType_C;
DEF_EXTERN jobject GiArrayType_ARRAY;
DEF_EXTERN jobject GiArrayType_PTR_ARRAY;
DEF_EXTERN jobject GiArrayType_BYTE_ARRAY;

DEF_EXTERN jclass GiDirection;
DEF_EXTERN jobject GiDirection_IN;
DEF_EXTERN jobject GiDirection_OUT;
DEF_EXTERN jobject GiDirection_INOUT;

DEF_EXTERN jclass GiFieldInfoFlags;
DEF_EXTERN jobject GiFieldInfoFlags_IS_READABLE;
DEF_EXTERN jobject GiFieldInfoFlags_IS_WRITABLE;

DEF_EXTERN jclass GiFunctionInfoFlags;
DEF_EXTERN jobject GiFunctionInfoFlags_IS_METHOD;
DEF_EXTERN jobject GiFunctionInfoFlags_IS_CONSTRUCTOR;
DEF_EXTERN jobject GiFunctionInfoFlags_IS_GETTER;
DEF_EXTERN jobject GiFunctionInfoFlags_IS_SETTER;
DEF_EXTERN jobject GiFunctionInfoFlags_WRAPS_VFUNC;
DEF_EXTERN jobject GiFunctionInfoFlags_THROWS;

DEF_EXTERN jclass GiVFuncInfoFlags;
DEF_EXTERN jobject GiVFuncInfoFlags_MUST_CHAIN_UP;
DEF_EXTERN jobject GiVFuncInfoFlags_MUST_OVERRIDE;
DEF_EXTERN jobject GiVFuncInfoFlags_MUST_NOT_OVERRIDE;
DEF_EXTERN jobject GiVFuncInfoFlags_THROWS;

DEF_EXTERN jclass GiInfoType;
DEF_EXTERN jobject GiInfoType_INVALID;
DEF_EXTERN jobject GiInfoType_FUNCTION;
DEF_EXTERN jobject GiInfoType_CALLBACK;
DEF_EXTERN jobject GiInfoType_STRUCT;
DEF_EXTERN jobject GiInfoType_BOXED;
DEF_EXTERN jobject GiInfoType_ENUM;
DEF_EXTERN jobject GiInfoType_FLAGS;
DEF_EXTERN jobject GiInfoType_OBJECT;
DEF_EXTERN jobject GiInfoType_INTERFACE;
DEF_EXTERN jobject GiInfoType_CONSTANT;
DEF_EXTERN jobject GiInfoType_INVALID_0;
DEF_EXTERN jobject GiInfoType_UNION;
DEF_EXTERN jobject GiInfoType_VALUE;
DEF_EXTERN jobject GiInfoType_SIGNAL;
DEF_EXTERN jobject GiInfoType_VFUNC;
DEF_EXTERN jobject GiInfoType_PROPERTY;
DEF_EXTERN jobject GiInfoType_FIELD;
DEF_EXTERN jobject GiInfoType_ARG;
DEF_EXTERN jobject GiInfoType_TYPE;
DEF_EXTERN jobject GiInfoType_UNRESOLVED;

DEF_EXTERN jclass GiScopeType;
DEF_EXTERN jobject GiScopeType_INVALID;
DEF_EXTERN jobject GiScopeType_CALL;
DEF_EXTERN jobject GiScopeType_ASYNC;
DEF_EXTERN jobject GiScopeType_NOTIFIED;

DEF_EXTERN jclass GiTransfer;
DEF_EXTERN jobject GiTransfer_NOTHING;
DEF_EXTERN jobject GiTransfer_CONTAINER;
DEF_EXTERN jobject GiTransfer_EVERYTHING;

DEF_EXTERN jclass GiTypeTag;
DEF_EXTERN jobject GiTypeTag_VOID;
DEF_EXTERN jobject GiTypeTag_BOOLEAN;
DEF_EXTERN jobject GiTypeTag_INT8;
DEF_EXTERN jobject GiTypeTag_UINT8;
DEF_EXTERN jobject GiTypeTag_INT16;
DEF_EXTERN jobject GiTypeTag_UINT16;
DEF_EXTERN jobject GiTypeTag_INT32;
DEF_EXTERN jobject GiTypeTag_UINT32;
DEF_EXTERN jobject GiTypeTag_INT64;
DEF_EXTERN jobject GiTypeTag_UINT64;
DEF_EXTERN jobject GiTypeTag_FLOAT;
DEF_EXTERN jobject GiTypeTag_DOUBLE;
DEF_EXTERN jobject GiTypeTag_GTYPE;
DEF_EXTERN jobject GiTypeTag_UTF8;
DEF_EXTERN jobject GiTypeTag_FILENAME;
DEF_EXTERN jobject GiTypeTag_ARRAY;
DEF_EXTERN jobject GiTypeTag_INTERFACE;
DEF_EXTERN jobject GiTypeTag_GLIST;
DEF_EXTERN jobject GiTypeTag_GSLIST;
DEF_EXTERN jobject GiTypeTag_GHASH;
DEF_EXTERN jobject GiTypeTag_ERROR;
DEF_EXTERN jobject GiTypeTag_UNICHAR;
