#include <gtest/gtest.h>
#include "jvmenv.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    JvmEnv::Instance = (JvmEnv*) ::testing::AddGlobalTestEnvironment(new JvmEnv);

    return RUN_ALL_TESTS();
}
