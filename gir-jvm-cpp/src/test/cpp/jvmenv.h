#include <gtest/gtest.h>
#include <jni.h>

class JvmEnv: public ::testing::Environment {
public:
    static JvmEnv* Instance;

    JavaVM* jvm;
    JNIEnv* env;

    void SetUp() override;
    void TearDown() override;
};
