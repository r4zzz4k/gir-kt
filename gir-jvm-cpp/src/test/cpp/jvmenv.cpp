#include "jvmenv.h"
#include "onload.h"

JvmEnv* JvmEnv::Instance = nullptr;

void JvmEnv::SetUp() {
    fprintf(stderr, "JvmTest::SetUp()\n");

    JavaVMOption* options = new JavaVMOption[1];
    options[0].optionString = "-Djava.class.path=/home/r4zzz4k/projects/gir-kt/gir-jvm/build/classes/kotlin/main";

    JavaVMInitArgs vmArgs;
    vmArgs.version = JNI_VERSION_1_8;
    JNI_GetDefaultJavaVMInitArgs(&vmArgs);
    vmArgs.nOptions = 1;
    vmArgs.options = options;
    vmArgs.ignoreUnrecognized = false;

    fprintf(stderr, "JvmTest::SetUp(): JNI_CreateJavaVM\n");
    auto createJvmResult = JNI_CreateJavaVM(&jvm, (void**)&env, &vmArgs);

    delete[] options;

    if(createJvmResult != JNI_OK) {
        fprintf(stderr, "Unable to create Java VM: %d\n", createJvmResult);
        throw std::runtime_error("Unable to create Java VM");
    }
    fprintf(stderr, "JvmTest::SetUp(): init\n");
    if(!init(env)) {
        fprintf(stderr, "Unable to initialize gi wrapper\n");
        throw std::runtime_error("Unable to initialize gi wrapper");
    }
    fprintf(stderr, "JvmTest::SetUp(): done\n");
}
void JvmEnv::TearDown() {
    fprintf(stderr, "JvmTest::TearDown()\n");
    auto unloadJvmResult = jvm->DestroyJavaVM();
    if(unloadJvmResult != JNI_OK) {
        fprintf(stderr, "Unable to unload Java VM: %d\n", unloadJvmResult);
        throw std::runtime_error("Unable to unload Java VM");
    }
    fprintf(stderr, "JvmTest::TearDown(): done\n");
}
