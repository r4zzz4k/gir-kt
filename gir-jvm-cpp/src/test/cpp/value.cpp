#include "jvmenv.h"

#include <cstdlib>
#include <glib-object.h>
#include "gi.h"

class GValueTests: public ::testing::Test {
protected:
    JNIEnv* env;

    void SetUp() override {
        env = JvmEnv::Instance->env;
    }
};

TEST_F(GValueTests, test_native_convert) {
    constexpr char const* expected = "5";
    constexpr gint actual = 5;

    GValue inVal = G_VALUE_INIT;
    g_value_init(&inVal, G_TYPE_STRING);
    GValue outVal = G_VALUE_INIT;
    g_value_init(&outVal, G_TYPE_INT);

    g_value_set_static_string(&inVal, expected);
    long converted = strtol(g_value_get_string(&inVal), nullptr, 10);
    g_value_set_int(&outVal, converted);

    ASSERT_EQ(g_value_get_int(&outVal), actual);
}

TEST_F(GValueTests, test_boolean) {
    constexpr bool expected = true;
    constexpr gboolean actual = expected;

    jobject inVal = env->CallStaticObjectMethod(Boolean, Boolean_valueOf, expected);
    GValue outVal = G_VALUE_INIT;
    ASSERT_TRUE(convertGiValue(env, inVal, &outVal));
    ASSERT_EQ(g_value_get_boolean(&outVal), actual);
}

TEST_F(GValueTests, test_int) {
    constexpr int expected = 8;
    constexpr gint actual = expected;

    jobject inVal = env->CallStaticObjectMethod(Integer, Integer_valueOf, expected);
    GValue outVal = G_VALUE_INIT;
    ASSERT_TRUE(convertGiValue(env, inVal, &outVal));
    ASSERT_EQ(g_value_get_int(&outVal), actual);
}

