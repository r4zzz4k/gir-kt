import org.gradle.api.internal.provider.DefaultPropertyState
import org.gradle.language.cpp.tasks.CppCompile
import org.gradle.nativeplatform.tasks.CreateStaticLibrary
import org.gradle.nativeplatform.tasks.LinkSharedLibrary
import org.gradle.util.GFileUtils
import java.io.ByteArrayOutputStream
import java.io.FileInputStream

plugins {
    id("cpp-library")
    id("cpp-unit-test")
    id("maven-publish")
}

repositories {
    maven(url = "https://repo.gradle.org/gradle/libs-snapshots-local/")
}

dependencies {
    //testImplementation("org.gradle.cpp-samples:googletest:1.9.0-SNAPSHOT")
}

val javaHome = System.getenv("JAVA_HOME")
//val javaLibDir = "$javaHome/lib/server/"
val javaLibDir = "$javaHome/jre/lib/amd64/server/"

library {
    linkage.set(listOf(Linkage.SHARED))

    binaries.configureEach {
        compileTask.get().apply {
            println("Task: $name")
            (targetPlatform.get() as NativePlatform).apply {
                println("\tPlatform: ${operatingSystem.name} [${architecture.name}]")
            }

            (toolChain.get() as NativeToolChain).apply {
                println("\tToolchain: ${name}, GCC-compatible: ${this is GccCompatibleToolChain}")

                if (this is GccCompatibleToolChain) {
                    println("\tAdding JAVA_HOME headers from $javaHome")
                    includes("$javaHome/include")
                    includes("$javaHome/include/linux")
                    compilerArgs.add("-std=c++14")
                    //compilerArgs.add("-std=c++03")
                }
            }
        }

        if (this is CppSharedLibrary) {
            linkTask.get().apply {
                (toolChain.get() as NativeToolChain).apply {
                    if (this is GccCompatibleToolChain) {
                        //linkerArgs.add("-std=c++14")
                        linkerArgs.add("-std=c++03")
                    }
                }
            }
        }
    }
}

unitTest {
    binaries.whenElementFinalized {
        val metadataFormat = org.gradle.api.internal.artifacts.ivyservice.ivyresolve.parser.ModuleMetadataParser.FORMAT_VERSION

        dependencies {
            implementation("org.gradle.cpp-samples:googletest_$metadataFormat:latest.integration")
        }
    }

    binaries.configureEach {
        compileTask.get().apply {
            (toolChain.get() as NativeToolChain).apply {
                if (this is GccCompatibleToolChain) {
                    includes("$javaHome/include")
                    includes("$javaHome/include/linux")
                    compilerArgs.add("-std=c++14")
                    //compilerArgs.add("-std=c++03")
                }
            }
        }

        if(this is CppTestExecutable) {
            linkTask.get().apply {
                (toolChain.get() as NativeToolChain).apply {
                    if(this is GccCompatibleToolChain) {
                        linkerArgs.add("-v")
                        //linkerArgs.add("-std=c++14")
                        linkerArgs.add("-std=c++03")
                        lib("$javaLibDir/libjvm.so")
                    }
                }
            }
        }
    }
}

tasks.withType<InstallExecutable> {
    //lib("$javaHome/lib/server/libjvm.so")
    // https://github.com/gradle/gradle/blob/bd70ffe/subprojects/platform-native/src/main/java/org/gradle/nativeplatform/tasks/InstallExecutable.java#L257
    doLast {
        val scriptFile = runScriptFile.get().asFile
        val script = GFileUtils.readFile(scriptFile)
                .replace("LD_LIBRARY_PATH=\"", "LD_LIBRARY_PATH=\"$javaLibDir:")
                .replace("DYLD_LIBRARY_PATH=\"", "DYLD_LIBRARY_PATH=\"$javaLibDir:")
        //println("######################## NEW SCRIPT ##########################\n$script")
        GFileUtils.writeFile(script, scriptFile)
    }
}

tasks.withType<RunTestExecutable> {
    dependsOn(tasks.getByPath(":gir-jvm:assemble"))
    //environment["LD_DEBUG"] = "libs"
}
afterEvaluate {
    configurations {
        forEach {
            println("CONFIG: $it:")
            it.allArtifacts.forEach {
                println("ART: ${it.name}, ${it.file.absolutePath}")
            }
        }
    }
}

tasks.withType<CppCompile> {
    macros["_GLIBCXX_USE_CXX11_ABI"] = "0"

    /*prop<NativePlatform>(targetPlatform).let {
        print("tasks.withType<CppCompile>; Platform: ")
        if(it != null) {
            println("name = ${it.name}; displayName = ${it.displayName}")
        } else {
            println("${it?.javaClass?.canonicalName}")
        }
    }
    prop<NativeToolChain>(toolChain).let {
        print("tasks.withType<CppCompile>; Toolchain: ")
        if(it != null) {
            if(it is Property<*>) {
                val it2 = it as Property<NativeToolChain>
                println("name = ${it2.get}; displayName = ${it.displayName}")
            }
            println("name = ${it.name}; displayName = ${it.displayName}")
        } else {
            println("$it")
        }
    }*/
    /*toolChain.map {

    }*/
    //if(toolChain is Gcc || toolChain is Clang) {
    /*    includes("${System.getenv("JAVA_HOME")}/include")
        includes("${System.getenv("JAVA_HOME")}/include/linux")
        compilerArgs.add("-std=c++14")*/
    //}
}
/*
inline fun <reified T> prop(input: Any?) =
        when(input) {
            null -> null
            is T -> input
            is Property<*> -> null as T?//(input as Property<T>)
            else -> error("Invalid property type")
        }
*/
/*tasks.withType<LinkSharedLibrary> {
    //if(toolChain is Gcc || toolChain is Clang) {
        linkerArgs.add("-std=c++14")
    //}
}*/
pkgConfig("gobject-introspection-1.0")

publishing {
    repositories {
        maven(url = "/tmp/mvn")
    }
}

fun pkgConfig(pkg: String) {
    tasks.withType<CppCompile> {
        execAndGetStdout("pkg-config", "--cflags", pkg).split(' ').forEach { compilerArgs.add(it) }
    }
    tasks.withType<LinkSharedLibrary> {
        execAndGetStdout("pkg-config", "--libs", pkg).split(' ').forEach { linkerArgs.add(it) }
    }
    tasks.withType<CreateStaticLibrary> {
        execAndGetStdout("pkg-config", "--static", pkg).split(' ').forEach { staticLibArgs.add(it) }
    }
    tasks.withType<LinkExecutable> {
        execAndGetStdout("pkg-config", "--libs", pkg).split(' ').forEach { linkerArgs.add(it) }
    }
}

fun execAndGetStdout(vararg commandLine: String): String =
        ByteArrayOutputStream().use {
            exec {
                commandLine(*commandLine)
                standardOutput = it
            }
            return it.toString().trim()
        }
