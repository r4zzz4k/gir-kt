package me.r4zzz4k.girkt.gir

expect open class GiBaseInfo {
    val infoType: GiInfoType
    val namespace: String
    val name: String?
    val attributes: Map<String, String>
    val container: GiBaseInfo
}

expect class GiCallbackInfo: GiBaseInfo

expect class GiArgInfo: GiBaseInfo {
    val closure: Int
    val destroy: Int
    val direction: GiDirection
    val ownershipTransfer: GiTransfer
    val scope: GiScopeType
    val type: GiTypeInfo
    val nullable: Boolean
    val callerAllocates: Boolean
    val optional: Boolean
    val isReturnValue: Boolean
    val skip: Boolean
}

expect class GiConstantInfo: GiBaseInfo {
    val type: GiTypeInfo
    fun value(): GiArgument
    fun free(value: GiArgument)
}

expect class GiFieldInfo: GiBaseInfo {
    // TODO fun get(mem: Pointer, value: GiArgumentPtr): Boolean
    // TODO set
    val flags: Set<GiFieldInfoFlags>
}

expect class GiPropertyInfo: GiBaseInfo {
    val flags: GParamFlags
    val ownershipTransfer: GiTransfer
    val type: GiTypeInfo
}

expect class GiTypeInfo: GiBaseInfo {
    val isPointer: Boolean
    val tag: GiTypeTag
    fun paramType(num: Int): GiTypeInfo // TODO where is param count?
    val iface: GiBaseInfo?
    val arrayLength: Int
    val arrayFixedSize: Int
    val isZeroTerminated: Boolean
    val arrayType: GiArrayType?
}

expect class GiValueInfo: GiBaseInfo {
    val value: Long
}
