package me.r4zzz4k.girkt.gir

import me.r4zzz4k.girkt.ktutils.Field

expect class GObjectRef
expect class GPointer
expect class GTypeRef

expect class GiArgument {
    val vBoolean: Boolean
    val vInt8: Int
    val vUint8: Int
    val vInt16: Int
    val vUint16: Int
    val vInt32: Int
    val vUint32: Long
    val vInt64: Long
    val vUint64: Long
    val vFloat: Float
    val vDouble: Double
    val vShort: Int
    val vUshort: Int
    val vLong: Long
    val vUlong: Long
    val vSsize: Long
    val vSize: Long
    val vString: String
    //val vPointer: String
}

class Ref<T>(@Field var ref: T)
