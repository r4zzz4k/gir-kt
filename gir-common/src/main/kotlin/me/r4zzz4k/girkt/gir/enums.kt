package me.r4zzz4k.girkt.gir

// TODO: replace with exceptions:
enum class GInvokeError {
    FAILED,
    SYMBOL_NOT_FOUND,
    ARGUMENT_MISMATCH,
}

enum class GParamFlags {
    READABLE,
    WRITABLE,
    READWRITE,
    CONSTRUCT,
    CONSTRUCT_ONLY,
    LAX_VALIDATION,
    STATIC_NAME,
    PRIVATE,
    STATIC_NICK,
    STATIC_BLURB,
    EXPLICIT_NOTIFY,
    DEPRECATED,
}

enum class GSignalFlags {
    RUN_FIRST,
    RUN_LAST,
    RUN_CLEANUP,
    NO_RECURSE,
    DETAILED,
    ACTION,
    NO_HOOKS,
    MUST_COLLECT,
    DEPRECATED,
}

enum class GiArrayType {
    C,
    ARRAY,
    PTR_ARRAY,
    BYTE_ARRAY,
}

enum class GiDirection {
    IN,
    OUT,
    INOUT,
}

enum class GiInfoType {
    INVALID,
    FUNCTION,
    CALLBACK,
    STRUCT,
    BOXED,
    ENUM,
    FLAGS,
    OBJECT,
    INTERFACE,
    CONSTANT,
    INVALID_0,
    UNION,
    VALUE,
    SIGNAL,
    VFUNC,
    PROPERTY,
    FIELD,
    ARG,
    TYPE,
    UNRESOLVED;

    override fun toString() = giInfoTypeToString(this)
}

enum class GiFieldInfoFlags {
    IS_READABLE,
    IS_WRITABLE,
}

enum class GiFunctionInfoFlags {
    IS_METHOD,
    IS_CONSTRUCTOR,
    IS_GETTER,
    IS_SETTER,
    WRAPS_VFUNC,
    THROWS,
}

enum class GiVFuncInfoFlags {
    MUST_CHAIN_UP,
    MUST_OVERRIDE,
    MUST_NOT_OVERRIDE,
    THROWS,
}

enum class GiScopeType {
    INVALID,
    CALL,
    ASYNC,
    NOTIFIED,
}

enum class GiTransfer {
    NOTHING,
    CONTAINER,
    EVERYTHING,
}

enum class GiTypeTag {
    VOID,
    BOOLEAN,
    INT8,
    UINT8,
    INT16,
    UINT16,
    INT32,
    UINT32,
    INT64,
    UINT64,
    FLOAT,
    DOUBLE,
    GTYPE,
    UTF8,
    FILENAME,
    ARRAY,
    INTERFACE,
    GLIST,
    GSLIST,
    GHASH,
    ERROR,
    UNICHAR;

    override fun toString() = giTypeTagToString(this)
}

internal expect fun giInfoTypeToString(thiz: GiInfoType): String
internal expect fun giTypeTagToString(thiz: GiTypeTag): String
