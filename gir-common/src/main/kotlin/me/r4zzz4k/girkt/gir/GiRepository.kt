package me.r4zzz4k.girkt.gir

import me.r4zzz4k.girkt.ktutils.Throws

class GError(domain: String, code: Int, message: String): RuntimeException("GError[$domain]: ($code) $message")

expect class GiRepository {
    @Throws
    fun require(namespace: String, version: String? = null, lazy: Boolean = false) // TODO: Implement GiTypelib

    fun getSharedLibraries(namespace: String): List<String>
    fun getInfos(namespace: String): List<GiBaseInfo>
}

expect fun giRepository(): GiRepository
