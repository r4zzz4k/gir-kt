package me.r4zzz4k.girkt.gir

import me.r4zzz4k.girkt.ktutils.Throws

expect open class GiCallableInfo: GiBaseInfo {
    val throws: Boolean
    val args: List<GiArgInfo>
    val callerOwns: GiTransfer
    val returnAttributes: Map<String, String>
    val returnType: GiTypeInfo
    val isMethod: Boolean
    val returnNullable: Boolean
}
expect class GiFunctionInfo: GiCallableInfo {
    val flags: Set<GiFunctionInfoFlags>
    val property: GiPropertyInfo?
    val symbol: String
    val vfunc: GiVFuncInfo?
    @Throws
    operator fun invoke(vararg args: Any?): Any? // TODO either split inout into two lists or use one list instead of three
    // TODO error quark
}
expect class GiSignalInfo: GiCallableInfo {
    val flags: GSignalFlags
    val classClosure: GiVFuncInfo?
    val trueStopsEmit: Boolean
}
expect class GiVFuncInfo: GiCallableInfo {
    val flags: GiVFuncInfoFlags
    val offset: Int
    val signal: GiSignalInfo?
    val invoker: GiFunctionInfo?
    // TODO address
    // TODO @Throws operator fun invoke()
}
