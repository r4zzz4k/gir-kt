package me.r4zzz4k.girkt.gir

expect open class GiRegisteredTypeInfo: GiBaseInfo {
    open val typeName: String
    open val typeInit: String
    val gType: GTypeRef
}
expect class GiEnumInfo: GiRegisteredTypeInfo {
    val values: List<GiValueInfo>
    val methods: List<GiFunctionInfo>
    val storageType: GiTypeTag
    val errorDomain: String?
}
expect class GiInterfaceInfo: GiRegisteredTypeInfo {
    val prerequisites: List<GiBaseInfo>
    val properties: List<GiPropertyInfo>
    val methods: List<GiFunctionInfo>
    fun findMethod(name: String): GiFunctionInfo?
    val signals: List<GiSignalInfo>
    fun findSignal(name: String): GiSignalInfo?
    val vfuncs: List<GiVFuncInfo>
    fun findVfunc(name: String): GiVFuncInfo?
    val constants: List<GiConstantInfo>
    val ifaceStruct: GiStructInfo
}
expect class GiObjectInfo: GiRegisteredTypeInfo {
    val abstract: Boolean
    val fundamental: Boolean
    val parent: GiObjectInfo?
    override val typeName: String
    override val typeInit: String
    val constants: List<GiConstantInfo>
    val fields: List<GiFieldInfo>
    val interfaces: List<GiInterfaceInfo>
    val methods: List<GiFunctionInfo>
    fun findMethod(name: String): GiFunctionInfo?
    //fun findMethodUsingInterfaces(name: String, ...): GiFunctionInfo?
    val properties: List<GiPropertyInfo>
    val signals: List<GiSignalInfo>
    fun findSignal(name: String): GiSignalInfo?
    val vfuncs: List<GiVFuncInfo>
    fun findVfunc(name: String): GiVFuncInfo?
    //fun findVfuncUsingInterfaces(name: String, ...): GiVFuncInfo?
    val classStruct: GiStructInfo
    //val refFunction: String?
    //val refFunctionPointer: void * (*GIObjectInfoRefFunction) (void *object);
    //val unrefFunction: String?
    //val unrefFunctionPointer: void (*GIObjectInfoUnrefFunction) (void *object);
    //val setValueFunction: String?
    //val setValueFunctionPointer: void (*GIObjectInfoSetValueFunction) (GValue *value, void *object);
    //val getValueFunction: String?
    //val getValueFunctionPointer: void * (*GIObjectInfoGetValueFunction) (GValue *value);
}
expect class GiStructInfo: GiRegisteredTypeInfo {
    val alignment: Int // gsize
    val size: Int // gsize
    val isGtypeStruct: Boolean
    val foreign: Boolean
    val fields: List<GiFieldInfo>
    val methods: List<GiFunctionInfo>
    fun findMethod(name: String): GiFunctionInfo?
}
expect class GiUnionInfo: GiRegisteredTypeInfo {
    val fields: List<GiFieldInfo>
    val methods: List<GiFunctionInfo>
    val discriminated: Boolean
    val discriminatorOffset: Int
    val discriminatorType: GiTypeInfo
    fun discriminator(field: Int): GiConstantInfo
    fun findMethod(name: String): GiFunctionInfo?
    val size: Int // gsize
    val alignment: Int // gsize
}
