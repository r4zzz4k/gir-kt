plugins {
    id("org.jetbrains.kotlin.platform.common")
}

dependencies {
    compile(kotlin("stdlib-common"))
}
