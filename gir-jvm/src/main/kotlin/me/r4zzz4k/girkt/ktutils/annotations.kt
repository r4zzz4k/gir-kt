package me.r4zzz4k.girkt.ktutils

import kotlin.jvm.Throws

actual typealias Field = JvmField
actual typealias Throws = Throws
actual typealias Static = JvmStatic
