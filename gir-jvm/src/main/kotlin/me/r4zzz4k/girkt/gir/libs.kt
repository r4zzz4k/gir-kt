package me.r4zzz4k.girkt.gir

actual fun load(path: String) = System.load(path)
actual fun loadLibrary(name: String) = System.loadLibrary(name)
