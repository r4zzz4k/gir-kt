package me.r4zzz4k.girkt.gir

actual open class GiRegisteredTypeInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual open val typeName: String external get
    actual open val typeInit: String external get
    actual val gType: GTypeRef external get
}

actual class GiEnumInfo internal constructor(ptr: Long): GiRegisteredTypeInfo(ptr) {
    actual val values: List<GiValueInfo> external get
    actual val methods: List<GiFunctionInfo> external get
    actual val storageType: GiTypeTag external get
    actual val errorDomain: String? external get
}

actual class GiInterfaceInfo internal constructor(ptr: Long): GiRegisteredTypeInfo(ptr) {
    actual val prerequisites: List<GiBaseInfo> external get
    actual val properties: List<GiPropertyInfo> external get
    actual val methods: List<GiFunctionInfo> external get
    actual external fun findMethod(name: String): GiFunctionInfo?
    actual val signals: List<GiSignalInfo> external get
    actual external fun findSignal(name: String): GiSignalInfo?
    actual val vfuncs: List<GiVFuncInfo> external get
    actual external fun findVfunc(name: String): GiVFuncInfo?
    actual val constants: List<GiConstantInfo> external get
    actual val ifaceStruct: GiStructInfo external get
}

actual class GiObjectInfo internal constructor(ptr: Long): GiRegisteredTypeInfo(ptr) {
    actual val abstract: Boolean external get
    actual val fundamental: Boolean external get
    actual val parent: GiObjectInfo? external get
    actual override val typeName: String external get
    actual override val typeInit: String external get
    actual val constants: List<GiConstantInfo> external get
    actual val fields: List<GiFieldInfo> external get
    actual val interfaces: List<GiInterfaceInfo> external get
    actual val methods: List<GiFunctionInfo> external get
    actual external fun findMethod(name: String): GiFunctionInfo?
    actual val properties: List<GiPropertyInfo> external get
    actual val signals: List<GiSignalInfo> external get
    actual external fun findSignal(name: String): GiSignalInfo?
    actual val vfuncs: List<GiVFuncInfo> external get
    actual external fun findVfunc(name: String): GiVFuncInfo?
    actual val classStruct: GiStructInfo external get
}

actual class GiStructInfo internal constructor(ptr: Long): GiRegisteredTypeInfo(ptr) {
    actual val alignment: Int external get // gsize
    actual val size: Int external get // gsize
    actual val isGtypeStruct: Boolean external get
    actual val foreign: Boolean external get
    actual val fields: List<GiFieldInfo> external get
    actual val methods: List<GiFunctionInfo> external get
    actual external fun findMethod(name: String): GiFunctionInfo?
}

actual class GiUnionInfo internal constructor(ptr: Long): GiRegisteredTypeInfo(ptr) {
    actual val fields: List<GiFieldInfo> external get
    actual val methods: List<GiFunctionInfo> external get
    actual val discriminated: Boolean external get
    actual val discriminatorOffset: Int external get
    actual val discriminatorType: GiTypeInfo external get
    actual external fun discriminator(field: Int): GiConstantInfo
    actual external fun findMethod(name: String): GiFunctionInfo?
    actual val size: Int external get // gsize
    actual val alignment: Int external get // gsize
}
