package me.r4zzz4k.girkt.gir

actual open class GiBaseInfo internal constructor(internal val ptr: Long) {
    actual val infoType: GiInfoType external get
    actual val namespace: String external get
    actual val name: String? external get
    actual val attributes: Map<String, String> external get
    actual val container: GiBaseInfo external get
}

actual class GiCallbackInfo internal constructor(ptr: Long): GiBaseInfo(ptr)

actual class GiArgInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual val closure: Int external get
    actual val destroy: Int external get
    actual val direction: GiDirection external get
    actual val ownershipTransfer: GiTransfer external get
    actual val scope: GiScopeType external get
    actual val type: GiTypeInfo external get
    actual val nullable: Boolean external get
    actual val callerAllocates: Boolean external get
    actual val optional: Boolean external get
    actual val isReturnValue: Boolean external get
    actual val skip: Boolean external get
}

actual class GiConstantInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual val type: GiTypeInfo external get
    actual external fun value(): GiArgument
    actual external fun free(value: GiArgument)
}

actual class GiFieldInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    // TODO fun get(mem: Pointer, value: GiArgumentPtr): Boolean
    // TODO set
    actual val flags: Set<GiFieldInfoFlags> external get
}

actual class GiPropertyInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual val flags: GParamFlags external get
    actual val ownershipTransfer: GiTransfer external get
    actual val type: GiTypeInfo external get
}

actual class GiTypeInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual val isPointer: Boolean external get
    actual val tag: GiTypeTag external get
    actual external fun paramType(num: Int): GiTypeInfo // TODO where is param count?
    actual val iface: GiBaseInfo? external get
    actual val arrayLength: Int external get
    actual val arrayFixedSize: Int external get
    actual val isZeroTerminated: Boolean external get
    actual val arrayType: GiArrayType? external get
}

actual class GiValueInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual val value: Long external get
}
