package me.r4zzz4k.girkt.gir

actual open class GiCallableInfo internal constructor(ptr: Long): GiBaseInfo(ptr) {
    actual val throws: Boolean external get
    actual val args: List<GiArgInfo> external get
    actual val callerOwns: GiTransfer external get
    actual val returnAttributes: Map<String, String> external get
    actual val returnType: GiTypeInfo external get
    actual val isMethod: Boolean external get
    actual val returnNullable: Boolean external get
}

actual class GiFunctionInfo internal constructor(ptr: Long): GiCallableInfo(ptr) {
    actual val flags: Set<GiFunctionInfoFlags> external get
    actual val property: GiPropertyInfo? external get
    actual val symbol: String external get
    actual val vfunc: GiVFuncInfo? external get
    @Throws actual external operator fun invoke(vararg args: Any?): Any? // TODO either split inout into two lists or use one list instead of three
    // TODO error quark
}

actual class GiSignalInfo internal constructor(ptr: Long): GiCallableInfo(ptr) {
    actual val flags: GSignalFlags external get
    actual val classClosure: GiVFuncInfo? external get
    actual val trueStopsEmit: Boolean external get
}

actual class GiVFuncInfo internal constructor(ptr: Long): GiCallableInfo(ptr) {
    actual val flags: GiVFuncInfoFlags external get
    actual val offset: Int external get
    actual val signal: GiSignalInfo? external get
    actual val invoker: GiFunctionInfo? external get
    // TODO address
    // TODO @Throws operator fun invoke()
}
