package me.r4zzz4k.girkt.gir

actual class GObjectRef internal constructor(val ptr: Long)
actual class GPointer internal constructor(val ptr: Long)
actual class GTypeRef internal constructor(val ptr: Long)

actual class GiArgument internal constructor(internal val ptr: Long) {
    actual val vBoolean: Boolean external get
    actual val vInt8: Int external get
    actual val vUint8: Int external get
    actual val vInt16: Int external get
    actual val vUint16: Int external get
    actual val vInt32: Int external get
    actual val vUint32: Long external get
    actual val vInt64: Long external get
    actual val vUint64: Long external get
    actual val vFloat: Float external get
    actual val vDouble: Double external get
    actual val vShort: Int external get
    actual val vUshort: Int external get
    actual val vLong: Long external get
    actual val vUlong: Long external get
    actual val vSsize: Long external get
    actual val vSize: Long external get
    actual val vString: String external get
    //actual val vPointer: String external get
}
