package me.r4zzz4k.girkt.gir

actual class GiRepository internal constructor(@JvmField internal val ptr: Long) {
    @Throws
    actual external fun require(namespace: String, version: String?, lazy: Boolean)

    actual external fun getSharedLibraries(namespace: String): List<String>
    actual external fun getInfos(namespace: String): List<GiBaseInfo>
}

actual external fun giRepository(): GiRepository
