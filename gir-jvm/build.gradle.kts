import org.gradle.language.cpp.CppLibrary
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
    id("org.jetbrains.kotlin.platform.jvm")
    id("java-library")
}
/*
java {
    sourceSets {
        getByName("main") {
            resources { files("${project(":gir-jvm-cpp").buildDir}/lib/main/release/libgir-jvm-cpp.so") }
        }
    }
}
*/
//tasks.getByPath("jar").dependsOn(project(":gir-jvm-cpp").task("releaseRuntimeElements"))

tasks {
    val processResources: ProcessResources by tasks
    processResources.apply {
        dependsOn(tasks.getByPath(":gir-jvm-cpp:assembleRelease"))
        from(project(":gir-jvm-cpp").configurations["releaseRuntimeElements"].artifacts.map { println(it.file); it.file })
    }

    /*val jar: Jar by tasks
    jar.apply {
        dependsOn(project(":gir-jvm-cpp").tasks.getByPath("releaseRuntimeElements"))
        doLast {
            project(":gir-jvm-cpp").configurations.get("releaseRuntimeElements").artifacts.forEach {
                println("JAR FROM: ${it.name}, ${it.file.absolutePath}")
            }
        }
        //from(project(":gir-jvm-cpp").configurations.get("releaseRuntimeElements"))
    }*/

    /*"dependent" {

        doLast {
            println("JAR:")
            //project(":gir-jvm-cpp").tasks.forEach { println(it.name) }
            project(":gir-jvm-cpp").configurations.forEach { println(it.name) }

            println("JAR:")
        }
    }*/
}

//tasks.findByPath("jar")?.dependsOn(project(":gir-jvm-cpp").tasks.findByPath("assembleRelease"))

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    expectedBy(project(":gir-common"))

    println("start")
    //project(":gir-jvm-cpp").getDependencyProject().getComponents().withType<CppLibrary>().forEach { println("ART ${it.getName()}") }
    //println("${project(":gir-jvm-cpp").getDependencyProject().}")
    //implementation()
    println("end")

    project(":gir-jvm-cpp").artifacts.forEach { println("ART ${it.name}") }//first { it.name }
    //implementation()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
