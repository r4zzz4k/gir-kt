package me.r4zzz4k.girkt.samples.simplegtk

import me.r4zzz4k.girkt.gir.*
import java.io.File
import java.util.*

fun GiRepository.loadNamespace(namespace: String, version: String): List<GiBaseInfo> {
    Log.debug("loadNamespace(): requiring $namespace-$version")
    require(namespace, version)
    //load("/home/r4zzz4k/projects/gir-kt/vala-sample/libhello.so")
    getSharedLibraries(namespace).forEach {
        // TODO fix this hack:
        Log.debug("Loading [$it]")
        loadLibrary(it.substring(3, it.lastIndexOf(".so")))
        //load("/usr/lib/x86_64-linux-gnu/$it") // loadLibrary(it)
    }
    return getInfos(namespace)
}

fun List<GiBaseInfo>.namedOrNull(name: String) = firstOrNull { it.name == name }
fun List<GiBaseInfo>.fnOrNull(name: String) = namedOrNull(name)?.let { it as GiFunctionInfo }
fun List<GiBaseInfo>.objOrNull(name: String) = namedOrNull(name)?.let { it as GiObjectInfo }

fun List<GiBaseInfo>.named(name: String) = firstOrNull { it.name == name } ?: error("Cannot find info named \"$name\"")
fun List<GiBaseInfo>.fn(name: String) = named(name).let { it as GiFunctionInfo }
fun List<GiBaseInfo>.obj(name: String) = named(name).let { it as GiObjectInfo }

fun List<GiBaseInfo>.fns() = filterIsInstance<GiFunctionInfo>()
fun List<GiBaseInfo>.objs() = filterIsInstance<GiObjectInfo>()
/*
class X {
    var p: Int = 5
}

fun x() {
    val x = X();
    val pp0 = X::p
    val pp1 = X::p
    val pp2 = x::p
}
*/
fun main(args: Array<String>) {
    Log.minLevel = Log.Level.VERBOSE

    //Log.debug("main(): PID ${ProcessHandle.current().pid()}")
    val nativeLibrary = File.createTempFile("lib", ".so").apply {
        outputStream().use { out ->
            Log::class.java.getResourceAsStream("/libgir-jvm-cpp.so")
                    .use { it.copyTo(out) }
        }
    }

    Log.debug("main(): loading library")
    Log.info("java.library.path: ${System.getProperty("java.library.path")}")
    load(nativeLibrary.absolutePath)

    val repo = giRepository()

    Log.debug("main(): loading GObject")
    val gobject = repo.loadNamespace("GObject", "2.0")
    val Object = gobject.obj("Object")
    val Object_new = Object.methods.fn("new")

    Log.debug("main(): loading Hello")
    val hello = repo.loadNamespace("Hello", "1.0")
    val hello_Person = hello.obj("Person")
    val hello_Person_say_hello = hello_Person.methods.fn("say_hello")

    val person = Object_new(hello_Person.gType, 0, arrayOf<String>(), arrayOf<Any?>()) as GObjectRef
    hello_Person_say_hello(person)

    val personNamed = Object_new(hello_Person.gType, 0, arrayOf<String>("name", "age"), arrayOf<Any?>("Василий", 32)) as GObjectRef
    hello_Person_say_hello(personNamed)

    //val glib = repo.loadNamespace("GLib", "2.0")
    //val assertion_message = glib.fn("assertion_message")
    //assertion_message("Санька", "Сашечка", 42, "Санёчек", "Я эту хуйню подсредством моего говна вывел!")

    Log.debug("main(): loading Gtk")
    val gtk = repo.loadNamespace("Gtk", "3.0")
    val gtk_MessageDilog = gtk.obj("MessageDialog")

    val arr = arrayOf<String>()

    val len = java.lang.reflect.Array.getLength(arr)
    println("Len: $len")

    /*val dialog = Object_new(gtk_MessageDilog.gType,
            2,
            arrayOf<String>(
                    "buttons",
                    "message-type"
            ),
            arrayOf<Any?>(
                    2,
                    2
            ))*/

    /*println("findMethod: ${gtk_MessageDilog.findMethod("new")?.name}")
    println("methods: ${gtk_MessageDilog.methods.fnOrNull("new")?.name}")*/
    //val dialog = GtkMessageDilog.methods.fn("new")(listOf(null, 0, 1, 2, Ref("ALLO YOBA ETO TU")))
    //val dialog = GtkMessageDilog.classStruct.methods.fn("new")(listOf(null, 0, 1, 2, Ref("ALLO YOBA ETO TU")))

    println("main(): end")
}
