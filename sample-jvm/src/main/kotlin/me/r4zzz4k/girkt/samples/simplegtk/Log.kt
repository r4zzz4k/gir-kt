package me.r4zzz4k.girkt.samples.simplegtk

import java.io.File
import java.text.SimpleDateFormat
import java.util.*

object Log {
    enum class Level(val label: String) {
        VERBOSE("V"),
        DEBUG("D"),
        INFO("I"),
        WARNING("W"),
        ERROR("E"),
    }

    private val file = File("/tmp/girkt.txt")
    var minLevel = Level.INFO

    init {
        file.writeText("")
    }

    private fun output(level: Level, message: Any?) {
        if(level.ordinal < minLevel.ordinal) {
            return
        }
        val timestamp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Date())
        val output = "$timestamp [${level.label}] $message\n"
        kotlin.io.print(output)
        file.appendText(output)
    }
    fun verbose(message: Any?) = output(Level.VERBOSE, message)
    fun debug(message: Any?) = output(Level.DEBUG, message)
    fun info(message: Any?) = output(Level.INFO, message)
    fun warning(message: Any?) = output(Level.WARNING, message)
    fun error(message: Any?) = output(Level.ERROR, message)
}
