import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

group = "me.r4zzz4k.girkt.samples"
version = "1.0-SNAPSHOT"

//apply(plugin('org.jetbrains.kotlin.platform.common'))
plugins {
    id("org.jetbrains.kotlin.platform.jvm")
    id("application")
    id("com.github.johnrengelman.shadow")
}

application {
    mainClassName = "me.r4zzz4k.girkt.samples.simplegtk.AppKt"
}

tasks.withType<ShadowJar> {
    baseName = "girkt-sample"
    classifier = null
    version = null
}

tasks.getByName<JavaExec>("runShadow") {
    environment("GI_TYPELIB_PATH", rootProject.file("vala-sample").absolutePath)
    environment("LD_LIBRARY_PATH", listOf(
            rootProject.file("vala-sample").absolutePath,
            "/usr/lib/x86_64-linux-gnu"
    ).joinToString(separator = ":"))
}

tasks {
    val processResources: ProcessResources by tasks
    processResources.apply {
        dependsOn(tasks.getByPath(":gir-jvm-cpp:assembleRelease"))
        from(project(":gir-jvm-cpp").configurations["releaseRuntimeElements"].artifacts.map { it.file })
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(project(":gir-common"))
    implementation(project(":gir-jvm"))
    implementation(project(":generated-gtk"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
