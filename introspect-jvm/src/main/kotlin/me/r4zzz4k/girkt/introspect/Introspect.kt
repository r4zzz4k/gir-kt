@file:JvmName("Introspect")
package me.r4zzz4k.girkt.introspect

import me.r4zzz4k.girkt.gir.*
import java.io.File
import com.xenomachina.argparser.ArgParser
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.nio.charset.CharsetEncoder
import java.nio.file.*

fun GiRepository.loadNamespace(namespace: String, version: String): List<GiBaseInfo> {
    Log.debug("loadNamespace(): requiring $namespace-$version")
    require(namespace, version)
    getSharedLibraries(namespace).forEach {
        // TODO fix this hack:
        Log.debug("Loading [$it]")
        load("/usr/lib/x86_64-linux-gnu/$it") // loadLibrary(it)
    }
    return getInfos(namespace)
}

fun String.toByteBuffer(): ByteBuffer = ByteBuffer.wrap(toByteArray())

fun introspectInfos(infos: List<GiBaseInfo>, namespaceDir: Path) {
    Log.debug("introspectInfos(): ${infos.size}")
    val types = mutableMapOf<Class<*>, Int>()
    infos.forEach {
        types.put(it.javaClass, types.getOrDefault(it.javaClass, 0) + 1)

        Log.info("[${it.namespace}]: (${it.infoType} | ${it.javaClass.simpleName}) ${it.name}")

        //Log.info("[${it.namespace}]: (${it.infoType} | ${it.javaClass.simpleName}) ${it.name}")
        /*if(it is GiFunctionInfo) {
            Log.println("[${it.namespace}]: (${it.infoType}) ${it.name}")
            Log.println("\tFlags: ${it.flags.joinToString { it.name }}")
            Log.println("\tSymbol: ${it.symbol}")
        }*/

        if(it is GiObjectInfo) {
            val packageName = namespaceDir.fileName

            val outFile = namespaceDir.resolve("${it.name}.kt")
            val outChan = Files.newByteChannel(
                    outFile,
                    setOf(
                            StandardOpenOption.WRITE,
                            StandardOpenOption.CREATE,
                            StandardOpenOption.TRUNCATE_EXISTING))

            //val utf8 = Charset.forName("UTF-8")
            //val encoder = utf8.newEncoder()
            outChan.write("package $packageName\n".toByteBuffer())
            outChan.write("\n".toByteBuffer())
            outChan.write("class ${it.name}() {\n".toByteBuffer())
            it.methods.forEach {
                outChan.write("\tfun ${it.name}(...)\n".toByteBuffer())
                /*Log.info("\t\t${it.name ?: "[not named]"}")
                Log.info("\t\t\tSymbol: ${it.symbol}")
                Log.info("\t\t\tFlags: ${it.flags.joinToString { it.name }}")*/
            }
            outChan.write("}\n".toByteBuffer())
            outChan.close()
        }
    }

    types.forEach { type, count -> Log.info("${type.simpleName}: $count") }
}

fun introspectEnums(infos: List<GiBaseInfo>) = infos.filterIsInstance<GiEnumInfo>().forEach {
    Log.info("Enum ${it.name}")

    val values = it.values
    Log.info("\tValues (${values.size}):")
    values.forEach {
        Log.info("\t\t${it.name ?: "[not named]"} = ${it.value}")
    }

    val methods = it.methods
    if(methods.isNotEmpty()) {
        Log.info("\tMethods (${methods.size}):")
        methods.forEach {
            Log.info("\t\t${it.name ?: "[not named]"}")
        }
    }

    val attributes = it.attributes
    if(attributes.isNotEmpty()) {
        Log.info("\tAttributes (${attributes.size}):")
        attributes.entries.forEach {
            Log.info("\t\t${it.key} = ${it.value}")
        }
    }
}

class Args(parser: ArgParser) {
    val namespaces by parser.adding(
            "-n", "--namespace",
            help = "Namespace with version, i.e. GLib-3.0") {
                this
            }
    val output by parser.storing(
            "-o", "--output",
            help = "Directory to store generated sources") {
                FileSystems.getDefault().getPath(this)
            }
}

fun main(argsArray: Array<String>) {
    Log.minLevel = Log.Level.VERBOSE
    val args = ArgParser(argsArray).parseInto(::Args)

    Log.debug("main()")
    val nativeLibrary = File.createTempFile("lib", ".so").apply {
        outputStream().use { out ->
            Log::class.java.getResourceAsStream("/libgir-jvm-cpp.so")
                    .use { it.copyTo(out) }
        }
    }

    Log.debug("main(): loading library")
    Log.info("java.library.path: ${System.getProperty("java.library.path")}")
    load(nativeLibrary.absolutePath)

    Log.debug("main(): getting repository")
    val repo = giRepository()

    val output = args.output.also { Files.createDirectories(it) }

    args.namespaces
            .map {
                it.split('-', ignoreCase = false, limit = 2).let { Pair(it[0], it[1]) }
            }
            .map { namespace ->
                val dir = output.resolve(namespace.first.toLowerCase())
                Files.createDirectories(dir)
                Pair(namespace, dir)
            }
            .map { (namespace, dir) ->
                val infos = repo.loadNamespace(namespace.first, namespace.second)
                Pair(infos, dir)
            }
            .forEach { (infos, dir) -> introspectInfos(infos, dir) }

    //introspectNamespace(repo, "Gtk", "3.0")
}
