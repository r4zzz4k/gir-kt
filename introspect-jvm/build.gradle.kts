import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

group = "me.r4zzz4k.girkt"
version = "1.0-SNAPSHOT"

//apply(plugin('org.jetbrains.kotlin.platform.common'))
plugins {
    id("org.jetbrains.kotlin.platform.jvm")
    id("application")
    id("com.github.johnrengelman.shadow")
}

application {
    mainClassName = "me.r4zzz4k.girkt.introspect.Introspect"
}

tasks.withType<ShadowJar> {
    baseName = "girkt-introspect"
    classifier = null
    version = null
}

tasks.getByName<JavaExec>("runShadow") {
    args = listOf(
            "--namespace", "GObject-2.0",
            "--namespace", "GLib-2.0",
            "--output", "/home/r4zzz4k/projects/gir-kt/generated-gtk/src/main/kotlin"
    )
}

tasks {
    val processResources: ProcessResources by tasks
    processResources.apply {
        dependsOn(tasks.getByPath(":gir-jvm-cpp:assembleRelease"))
        from(project(":gir-jvm-cpp").configurations["releaseRuntimeElements"].artifacts.map { it.file })
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(project(":gir-common"))
    implementation(project(":gir-jvm"))
    implementation("com.xenomachina:kotlin-argparser:2.0.7")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
