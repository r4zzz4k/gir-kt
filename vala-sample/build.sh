#!/bin/sh

mkdir build
cd build
valac ../hello.vala --gir=Hello-1.0.gir --library Hello-1.0 -H hello.h -X -fPIC -X -shared -o libhello.so
g-ir-compiler --shared-library libhello.so --output=Hello-1.0.typelib Hello-1.0.gir
