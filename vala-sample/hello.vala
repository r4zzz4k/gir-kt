namespace Hello {
    public struct Point {
        public int x;
        public int y;
    }

    public class Person: Object {
        public string name { get; set; default = "He Who Must Not Be Named"; }
        public int age { get; set; default = 42; }
        public Person(string name) {
            this.name = name;
        }

        public void say_hello() {
            stderr.printf("%s of age %d says hello!\n", this.name, this.age);
        }
    }
}
/*
public int main() {
    var point = Hello.Point() { x = 1, y = 2 };
    stdout.printf("%d %d\n", point.x, point.y);

    var person = new Hello.Person("Василий Пупкин");
    stdout.printf("%s\n", person.name);

    return 0;
}
*/
